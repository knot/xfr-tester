# XFR Tester

XFR Query Tester

Utility for testing correct processing of XFR responses and DNS NOTIFY queries.

## Setup

Firstly, configure the secondary server. For example, _knot_:

```yaml
# Example of a very simple Knot DNS configuration.
server:
    listen: 127.0.0.1@5301

remote:
  - id: master
    address: 127.0.0.1@5302

acl:
  - id: notify_from_master
    address: 127.0.0.1
    action: notify
  - id: transfer_from_client
    address: 127.0.0.1
    action: transfer

zone:
  - domain: test.xfr
    file: test-xfr.zone
    master: master
    acl: [notify_from_master, transfer_from_client]
```

Then, install this app into a virtual environment and prepare test scenarios and configs.

```commandline
$ pip install -r requirements.txt
```

Some test scenarios and config can be found in `default_tests`.

## Testing

Testing is then executed via `python xfr_tester` command. It has two important options:
    
1. `-c, --config` for passing the whole config module.
1. `-t, --test-case` for passing just a `TestCase` instance.

Let's run a basic `BasicARecordsTestCase` test case:

```commandline
$ python xfr_tester -t default_tests.tests.basicARecords
```

If some configuration is needed, there are other options to adjust the configuration.

```commandline
$ python xfr_tester --domain example.xfr --server-ip 127.0.0.2 --server-port 53 -t default_tests.tests.basicARecords
```

This and much more can be also configured in a config module.

```commandline
$ python xfr_tester -c default_tests.config -t default_tests.tests.basicARecords
```

The config module can also contain the `test_case` attribute. Then there is no need to explicitly specify the test.


```commandline
$ python xfr_tester -c module.config.with.test_case
```

For further details, read the `--help`.

```commandline
$ python xfr_tester -h
Usage: xfr_tester [OPTIONS]

  Test correct processing of zone transfers on a secondary DNS server.

  ...

Options:
  -h, --help                     Show this message and exit.
  --version                      Show the version and exit.
  -c, --config MODULE            Path to a whole config module. It's
                                 attributes are then passed to the Core as
                                 arguments.

  -t, --test-case TEST PATH      Path to a TestCase instance.
  -d, --domain DOMAIN            Domain used for testing. Default: test.xfr.
  --random-seed TEXT             Number used as a random seed for default
                                 argument values. Default: None

  --client-ip TEXT               IP address of the server under test. Default:
                                 127.0.0.1

  --client-port INTEGER          Port of the server under test. Default: 5302
  --client-na-name TEXT          Title of the default client NetworkAnalyst.
                                 Default: Client

  --server-ip TEXT               IP address of the testing server. Default:
                                 127.0.0.1

  --server-port INTEGER          Port of the testing server. Default: 5301
  --server-na-name TEXT          Title of the default server NetworkAnalyst.
                                 Default: Server

  --notify-soa-prob FLOAT RANGE  Probability of adding a SOA record to the DNS
                                 NOTIFY message. Default: 0.5
```

## Further details

Further details of the parameters and stuff can be found in the documentation. Unfortunately, the only available 
documentation at the moment is the one in the code.
