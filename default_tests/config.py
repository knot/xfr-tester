# SPDX-License-Identifier: MPL-2.0
from random import Random

from dns.name import Name

from xfr_tester.client import TcpClient
from xfr_tester.datatypes import IPAddress
from xfr_tester.network_analyst import NetworkAnalyst
from xfr_tester.notify_manager import DefaultNotifyManager
from xfr_tester.server import TcpServer

###################
# Global settings #
###################


# Base domain we want to use for testing.
domain = Name(('test', 'xfr', ''))

# Random instance to be able to replicate exactly the same test cases over and over.
rand = Random(24)

# Client - the tested secondary DNS server.
client = TcpClient(IPAddress('127.0.0.1', 5302), NetworkAnalyst('Client'))

# NotifyManager generates the DNS NOTIFY messages. We have only one manager, so we use it.
notify_manager = DefaultNotifyManager(domain, 0.5, rand=rand)

# Class managing the server communication
server_cls = TcpServer

# IP address, where the test server runs
test_server_ip = IPAddress('127.0.0.1', 5301)

# Network analyst for the server part
server_network_analyst = NetworkAnalyst('Server')
