# SPDX-License-Identifier: MPL-2.0
import os
from pathlib import Path
from typing import List

from dns.name import Name

from xfr_tester.test_case import (BasicARecordsTestCase, ComplexTestCase, JournalTestCase,  # noqa: F401
                                  MultiIncrementalTestCase, NameOcclusionTestCase, SerialNumberArithmeticTestCase,
                                  SingleRecordAxfrTestCase, SingleRecordTestCase, SOAChangesOnlyTestCase,
                                  XfrBlackoutTestCase)
from xfr_tester.test_case.abstract import AbstractRecordGenerator
from xfr_tester.test_case.complex.records_generator import (ARecordsGenerator, MaxDiffSoaRecordGenerator,  # noqa: F401
                                                            MXRecordsGenerator, NSRecordsGenerator,
                                                            OneByOneSoaRecordGenerator, UnknownRecordsGenerator)
from xfr_tester.test_case.journal import kjournalprint_reader

from .config import domain, rand

#########
# Tests #
#########


# BasicARecordsTestCase
basicARecords = BasicARecordsTestCase(domain=domain, iterations=10, rand=rand)

# MultiIncrementalTestCase
multiIncremental = MultiIncrementalTestCase(domain=domain, iterations=10, rand=rand)

# SOAChangesOnlyTestCase
soaChangesOnly = SOAChangesOnlyTestCase(domain=domain, iterations=10, rand=rand)

# NameOcclusionTestCase(domain, 10, rand)
nameOcclusion = NameOcclusionTestCase(domain=domain, iterations=10, rand=rand)

# SerialNumberArithmeticTestCase
serialNumberArithmetics = SerialNumberArithmeticTestCase(
    domain=domain, start=2 ** 30, step=2 ** 31 - 1, iterations=10, rand=rand)

# SingleRecordAxfrTestCase
singleRecordAxfrTestCase = SingleRecordAxfrTestCase(
    domain=domain, soa_records_generator=OneByOneSoaRecordGenerator(domain=domain), rand=rand)

# SingleRecordTestCase
records = [ARecordsGenerator(domain=domain, rand=rand).next_add()]
singleRecordTestCase = SingleRecordTestCase(
    domain=domain, soa_generator=OneByOneSoaRecordGenerator(domain=domain), records=records, rand=rand)

# JournalTestCase
reader = kjournalprint_reader(Path(os.path.join(os.path.dirname(__file__), 'example.journal')))
journalTestCase = JournalTestCase(domain=domain, reader=reader, rand=rand)

# ComplexTestCase
generators: List[AbstractRecordGenerator] = [
    ARecordsGenerator(domain=domain, rand=rand),
    ARecordsGenerator(domain=Name(('dynamic', 'sub',)).concatenate(domain), rand=rand),
    ARecordsGenerator(domain=Name(('static', 'sub',)).concatenate(domain), rand=rand, randomize_names=False),
    MXRecordsGenerator(domain=domain, rand=rand),
    MXRecordsGenerator(domain=domain, rand=rand, preference=2000, subdomain='mail2'),
    NSRecordsGenerator(domain=domain, rand=rand),
    UnknownRecordsGenerator(randomize_names=False, domain=domain, rand=rand),
]
complexTest = ComplexTestCase(
    domain=domain,
    iterations=20,
    soa_generator=OneByOneSoaRecordGenerator(domain=domain),
    record_generators=generators,
    rand=rand,
    min_increments_per_step=1,  # how many increments to do during one test iteration
    max_increments_per_step=5,
    min_remove_records_per_increment=1,  # how many records to remove during one increment
    max_remove_records_per_increment=5,
    min_new_records_per_increment=1,  # how many records to add during one increment
    max_new_records_per_increment=5
)

# XfrBlackoutTestCase is a wrapper which takes another TestCase and imitates net blackout
b_basicARecords = XfrBlackoutTestCase(basicARecords)
b_multiIncremental = XfrBlackoutTestCase(multiIncremental)
b_soaChangesOnly = XfrBlackoutTestCase(soaChangesOnly)
b_nameOcclusion = XfrBlackoutTestCase(nameOcclusion)
b_serialNumberArithmetics = XfrBlackoutTestCase(serialNumberArithmetics)
b_complexTest = XfrBlackoutTestCase(complexTest)
b_singleRecordTestCase = XfrBlackoutTestCase(singleRecordTestCase)
b_journalTestCase = XfrBlackoutTestCase(journalTestCase)
b_singleRecordAxfrTestCase = XfrBlackoutTestCase(singleRecordAxfrTestCase)
