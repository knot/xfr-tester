#!/usr/bin/python
# -*- coding: utf-8 -*-
# SPDX-License-Identifier: MPL-2.0
import setuptools

PROJECT_NAME = 'xfr-tester'
VERSION = '0.0.1'

INSTALL_REQUIRES = open('requirements.txt').read().splitlines()

with open('README.md', 'r') as fh:
    long_description = fh.read()


def main():
    setuptools.setup(
        name=PROJECT_NAME,
        version=VERSION,
        author='Daniel Hubáček',
        author_email='daniel.hubacek@nic.cz',
        description='Utility for testing correct processing of XFR responses and DNS NOTIFY queries.',
        long_description=long_description,
        long_description_content_type='text/x-rst',
        url='https://gitlab.nic.cz/knot/xfr-tester',
        packages=setuptools.find_packages(),
        classifiers=[
            'License :: OSI Approved :: GNU General Public License v3 or later (GPLv3+)',
            'Operating System :: OS Independent',
            'Programming Language :: Python',
            'Programming Language :: Python :: 3.6',
            'Programming Language :: Python :: 3.7',
            'Programming Language :: Python :: 3.8',
            'Topic :: Internet :: Name Service (DNS)',
            'Topic :: Software Development :: Testing',
        ],
        python_requires='>=3.6',
        install_requires=INSTALL_REQUIRES,
    )


if __name__ == '__main__':
    main()
