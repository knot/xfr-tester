% SPDX-License-Identifier: MPL-2.0
\chapter{Basics of DNS}

This thesis is dealing with the distributivity of \gls{DNS} servers, specifically with a process of zone changes transfer, where changes made on a primary server must be also replicated on secondary servers.

The goal is to create a testing \gls{DNS} server, which is generating some zone changes and subsequent propagation of these changes to a secondary server. Each change should focus on a possibly critical scenario, which could be processed incorrectly on the secondary \gls{DNS} server side. In the end, this testing primary server verifies the correctness of the zone transfer result and analyzes the network load.

This chapter describes the basics of \gls{DNS} - what are domain names, why and how did they come into existence, and why are they actually so useful. Then, more advanced issues about distributivity are described - why is it so important, how is it solved, and which troubles can occur. Details and specifications of zone transfer protocols are described at the end of this chapter.

It should be a sufficient introduction into the world of \gls{DNS}, which is necessary for understanding the issues described in the thesis.

\section{Origin of DNS}

Computer networks can contain tens, hundreds, or millions of interconnected devices. Nowadays it is not just about computers, but also cameras, televisions, watches, fridges, light bulbs, vacuum cleaners and so on. Almost every piece of electronics can be designed or made to communicate with its surroundings and together with other devices, they create networks of any size. The connection between smartwatches and a mobile phone, internet search engine, both are based on a communication between some devices.

To be possible to send some messages between particular devices, these messages must be addressed to someone. Therefore every device must have an address within a network. In case of \gls{IP}, it can be an IPv4 address, for example \textit{192.168.0.99}. Using an address, every device can determine whether or not does a message belong to it and if the device should send the message somewhere else or just ignore it. These addresses are simple to work with, for computers. It is hard to even remember for people.

That is the reason why devices started to be named with more human-readable names. One simple file has been created and this file contained pairs of name-address. The computer always went through this mapping file and found an address ("readable" for computer), which corresponded to a name (readable for humans).

Note that this file can be still found on today's devices. It is stored in \texttt{/etc/hosts} for UNIX systems or in \texttt{\%SystemRoot\%\textbackslash system32\textbackslash drivers\textbackslash etc\textbackslash hosts} for Windows. It is used as a static translator of domain names with the highest priority.

In the beginning, when every network was composed of a few devices, it was a sufficient solution. Changes in networks were rather rare, it could be managed by one person or there was a mechanism, which synchronized this file with another, remote, file - a database. But this solution was not sufficient for a long time. There were more and more devices in particular networks and these networks started to be also mutually connected. This \textit{file domain system} was not sufficient anymore, it was unbearable to manage so large networks with this primitive system.

In 1983, an American computer scientist Paul Mockapetris proposed in RFC 882 \cite{rfc882} and RFC 883 \cite{rfc883} a \gls{DNS} protocol, which was supposed to suit all requirements and solve all the problems, so primarily fast management of resource records - adding and removing. Four years later, in 1987, \gls{DNS} protocol has been updated in RFC 1034 \cite{rfc1034} and RFC 1035 \cite{rfc1035} into the form, which is still in effect now - after more than 30 years. After a huge increase of computational resources, number of devices, and also expectations of users, this protocol is still sufficient.

\section{How does DNS work}

The fundamental block of domain names is the hierarchy. The domain \textit{fel.cvut.cz} can be used as an illustration. Dots in this domain name represent individual parts or labels. It is quite intuitive that the first label, \textit{cz}, stays for the Czech republic. Second label, \textit{cvut}, represents the Czech Technical University (\textbf{Č}eské \textbf{V}ysoké \textbf{U}čení \textbf{T}echnické), and the last label, \textit{fel}, represents the faculty of electrical engineering (\textbf{f}akulta \textbf{el}ektrotechnická). There is the hierarchy, where every label somehow makes the previous label more specific. And this hierarchy is present even in the core of the \gls{DNS}, as it is described in this chapter.

This whole hierarchy of domain names can be understood as a tree, wherein every vertex there is one part of the domain (for example \textit{cz}) and from this vertex then go edges to other, subordinate, vertexes (for example \textit{cvut.cz}) and other subordinate vertexes (\textit{fel.cvut.cz}). So every vertex has its own sub-tree, which contains all the sub-domains. On the very top of this imaginary tree there must be one more vertex, which would roof all the first-labels, so-called \textit{top-level domains}, as is \textit{cz} or \textit{sk}, \textit{com}, \textit{net} and so on. This very first vertex is called \textit{root}. The imaginary hierarchical tree is illustrated in the \figref{fig:dns-hierarchy}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.8\linewidth]{figures/dns-basics/dns-hierarchy.pdf}
	\caption{DNS hierarchy example with \textit{fel.cvut.cz}}
	\label{fig:dns-hierarchy}
\end{figure}

Note that this root vertex can be explicitly inserted into a domain name by adding one dot at the end - \textit{fel.cvut.cz\textbf{.}}. After the last comma, it is possible to imagine an empty string, which represents exactly this root. Domain names without the ending dot are considered as \textit{relative} to a context. It is used mostly in configuration files.

Every vertex contains (next to the domain name) also some resource records. One of them is for example the IP address, or information about email services, server applications, contact on the domain administrator, and so on. Actually, it is possible to insert here any information. Every type of information has some sort of identifier, \textit{rdtype} (resource data type). For example, IPv4 addresses are called A records, IPv6 addresses are AAAA records, mail servers have \gls{MX} records, SRV for service locators, PTR for domain name pointers (useful for reverse lookups - find a domain by an address), NS for authoritative name servers, or TXT for any other text data. There are many other rdtypes defined in different \gls{RFC} documents, according to their usage. In all messages, these rdtypes have 2 bytes reserved, so it is a number from the interval 0..65535, which is large enough for all the types of records.

This distribution of domain names to some vertexes of an imaginary tree is actually a really strong approach, which allows delegating all the management of particular sub-trees to particular, independent, servers. The whole top-level domain \textit{cz} can be managed completely independently from other top-level domains, moreover, it can delegate a part of itself (for example \textit{cvut}) to another server. This delegation mechanism can be recursively repeated over and over again.

Every domain, every vertex, contains some information (in the form of records) about itself and the rest about its subordinates delegates (or to be exact - can delegate) to another server, without any dependencies on itself or its predecessors. Every vertex has a \gls{DNS} server. This server has all the records of a domain and some records of some sub-domains or can delegate some sub-domains to another server. This imaginary part of the hierarchical tree, which is managed by the \gls{DNS} server, is called a \textit{zone}. So a zone is a part of a sub-tree, part of the whole \gls{DNS} space.

The whole \gls{DNS} space is divided into zones and every server can delegate a part of a zone to another server. And this is exactly the way, how it works during resolving (resolving is a process when a resolver looks at some particular records of a domain). The resolver asks a server and receives either the answer or a hint, where to ask. When resolving the \textit{fel.cvut.cz} domain, the first query goes to a root. The root does not know the answer, but knows, where to find the \textit{cz} domain. The second query goes to the \textit{cz} domain, but neither this server knows the desired records, so it answers with a hint where is \textit{cvut.cz}. So the third query goes to the \textit{cvut.cz}, which answers with a hint about \textit{fel.cvut.cz} and finally, this server knows the answer, so it answers with the IPv4 address - with the A record. This process is illustrated in the \figref{fig:resolving} (shortened).

\begin{figure}[htp]
    \centering
    \inputminted{console}{figures/dns-basics/resolving.console}
    \caption{Resolving process of \textit{fel.cvut.cz}.}
    \label{fig:resolving}
\end{figure}

This process might seem to be quite long and annoying. Therefore there are \gls{DNS} resolvers, which do this work and return just the final answer. These resolvers are usually pre-installed in all operation systems. Also, all the records can be cached, so it is not necessary to execute this whole recursive process every time.

\section{Distributivity of DNS}

It has been already explained, that \gls{DNS} was created for the possibility of naming some devices in a readable, easily memorable, and meaningful way. It has been also explained how does resolving work and how does domain (or zone) management work. Or almost. There is still one thing, which is important in computer science and it is speed!

When a computer is located in Prague and tries to resolve the \textit{fel.cvut.cz} domain, it is all right and fast. But when a computer is located on the other side of the world, it takes some more time.

One of the important aspects is the distance from a \gls{DNS} server. It is obvious, that the best location of a server managing the \textit{cz} domain is the Czech Republic and also that servers of the Czech Technical University should be somewhere in Prague. The overwhelming majority of clients is from the Czech Republic so the average latency is minimized. If all the mentioned servers were only in the Czech Republic, the latency on the other side of the world would be significant and even increased by the number of queries, which has to be actually sent - query for \textit{cz.}, \textit{cvut.cz.}, \textit{fel.cvut.cz.} and also the final \gls{HTTP} query of a web browser. All 4 messages would go all over the world and back, therefore the latency would significantly increase. And this latency might be increasing linearly with every other label, with every other sub-domain.

Latency from a server can be reduced only by a shorter path between the client and the server, or by upgrading the communication technologies. It is not really possible to move the client nor the server, and it is not really possible to upgrade for example a metal wiring to optics. But it is possible to duplicate the server to somewhere else. Somewhere closer to other clients. Then, when a resolver asks a root server for the servers of \textit{cz} domain, the root server could return a list of servers and the resolver might choose the best one of them (the most reliable and fastest one based on some statistical data).

Another important advantage of server duplication is stability. When, for some reason, a server becomes unavailable, there are other servers with all the important information (all the resource records) to provide the service. The latency would be higher, but the service would still work, which is also a great way, how to prevent some types of attacks, such as \gls{DoS} or \gls{DDoS}, when the attacker tries to flood a victim with tons of requests so it is not possible to answer to other, regular, clients.

It can be noticed that in this situation (when a zone is distributed over multiple servers) there are multiple servers, from which the very same behaviour and very same answers are expected. The zone administrator is supposed to manage multiple servers. It is actually the same problem, which also had the simple system with one \textit{hosts} file before - the need to replicate a database across multiple servers. But \gls{DNS} has a solution for it.

Servers of a zone are either \textit{primary} or \textit{secondary}. A primary server is a server, which provides information about zone changes to other servers. A secondary server is a server, which asks another server about these changes. These definitions are not contradictory, which means that one server can be primary and secondary at the same time:

Let have three servers \texttt{A}, \texttt{B} and \texttt{C}. A zone administrator makes some changes in the server \texttt{A}. This server propagates the changes into the server \texttt{B}. The server \texttt{B} applies the changes and furthermore, it propagates the changes even to the server \texttt{C}. So the server \texttt{B} was the one, who accepts the changes and at the same time provides changes to another server.

This variant is valid. When the servers are configured properly it is needed to perform the changes only in one server and they are thereafter propagated even to other servers.

\section{Zone replication between servers}

In the first place, there is one record type named \gls{SOA}. It is a record, which must be present in every zone exactly once. Its value contains some important information about the whole zone - email address of a responsible administrator; serial number of the zone (kind of a zone version); an amount of time, when a secondary server should ask for zone changes; an amount of time, after which the server should try to contact the primary server again in case the primary server had not answered; an amount of time, after which the server should stop providing the answers, because they may not be in force anymore.

Already the \gls{SOA} record contains some information, which determines when should a secondary server ask for changes and how should the secondary server behave. But this \textit{refresh} time can be long and the primary server may not be available at the time when the secondary server is asking for the changes. So the secondary server may not provide current answers quite a long time after a zone has been officially changed. Another situation would be when the \gls{DNS} servers are logically connected in a chain (as exampled above with the \texttt{A}, \texttt{B} and \texttt{C} servers). It would take some time until the changes would be propagated to the end of this chain, so the answers would differ depending on which server is being queried.

That was the motivation while designing a mechanism named \textit{DNS NOTIFY}, which is described in the RFC 1996 \cite{rfc1996}. It is a simple way, how to indicate new zone changes to a secondary server. When a \gls{DNS} server updates its content (straightly by an administrator or automatically according to a primary server), it sends a Notify message about it to all its secondary servers. It is expected that these secondary servers are going to update their version of the zone as well, so they have actual resource records and the whole zone remains stable all over the servers.

It is important to be aware of the fact that reception of a Notify message does not necessarily mean a need for the zone refresh. Imagine a situation, when a secondary server has two primary servers. Both of them send a Notify message, but one message arrives later for some reason. Meanwhile, the secondary server has received the Notify message from the other primary server and updated its zone. When the delayed Notify message is received, there is no need to update the zone anymore since the zone version is actual.

The naive implementation is to pull the whole zone every time a Notify message is received. It is certain that the zone is always correct. The transfer of the whole zone follows the \gls{AXFR}, which is described in the RFC 5936 \cite{rfc5936}. But as it has been described, the zone might have been already updated and therefore the zone transfer might be completely superfluous.

A small improvement of the naive implementation is to check the zone version (the serial number of the \gls{SOA} record) first. A simple query for the \gls{SOA} record is sent to the primary server. If the received \gls{SOA} record has a greater serial number than the \gls{SOA} record of the secondary server, a zone transfer should be executed.

Zones might be really large, so a transfer of a whole zone might take some time and also might excessively load down the network. For illustration, 1. 1. 2021 the top-level domain \textit{cz} contained 1 370 749 registered domains (source: \url{https://stats.nic.cz}). It is highly ineffective to transfer over 1 million records after an addition of one new record, just to replicate the zone. That was the motivation for the \gls{IXFR} protocol, which is specified in the RFC 1995 \cite{rfc1995}.

Incremental zone transfer consists only of the changes between particular zone versions. If one new domain is added into a zone, the incremental zone transfer contains exactly this (new) record. The rest of the (unchanged) records is not included in the message, which saves some computational time and also reduces the network load. But even this way of transferring zone changes has some disadvantages, which might not be really obvious.

To be able to send only the changes between particular zone versions, it is needed to know these changes. So the server must have some sort of a \textit{log} (often called \textit{journal}), which contains all the information about the last few changes. A secondary server might not be available at the time (for example because of some blackout, attack, and so on) and it might ask for changes after a longer time, after a few more zone changes. If the primary server does not remember all the incremental changes anymore, it is forced to send the whole zone via \gls{AXFR}.

And also, the application of the incremental changes may not be processed totally correctly, which would have fatal consequences! There would be two different zone versions, which would be considered the same, and answers on queries might be strongly inconsistent depending on which server is being asked. These errors are hard to find or debug, and moreover - the server is not able to detect them. The only two situations, how to miraculously fix this error, are:

\begin{enumerate}
    \item Other incremental changes in all affected records, which would the secondary server be able to apply.
    \item Pull the whole zone via \gls{AXFR}. But that is what the secondary server is trying to avoid.
\end{enumerate}

It is a trap, which is not easily detectable and the server is not really able to fix it by itself. Therefore it is important to implement the zone transfers perfectly, especially the incremental changes application, and this implementation must be thoroughly tested.

\section{Details of the zone transfer protocols} \label{sec:details-of-the-zone-transfer-protocols}

Like every other communication protocol, even \gls{DNS} has its own format for messages, which has been described in the RFC \cite{rfc1035}. The message is divided into 5 sections - header, Question, Answer, Authority and Additional. Not every section has to contain some data. In certain cases, some sections, except the header, might remain empty. It is fully dependent on the type of the message, what information is the message supposed to carry. The zone transfer is essential for this project, therefore this type of message is going to be described more deeply in this section.

Firstly, it is useful to describe the header which is the same for all the messages. It consists of 6 parts, where each part is 16 bits, so in total it is 96 bits long header section. 

The first part of the header contains an ID using which it is possible to recognize answers to a particular question. The ID in an answer must be the same as in the question corresponding to the answer. In questions or queries, there can be any number. It is up to the client, which ID will be chosen to distinguish particular answers.

The second part of the header is reserved for flags and codes. Each flag and code is deeply described in the RFC \cite{rfc1035}. At this point, most of the header flags are not important and therefore their explanation will be skipped.

The last 4 parts of the header section contain numbers (unsigned 16-bit integer) of entries (resource records) in the corresponding sections (Question, Answer, Authority, Additional) of the message.

The message header format is displayed in \figref{fig:dns-message-header}.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/dns-basics/dns-header.ascii}
    \caption{DNS message header. \cite{rfc5936}}
    \label{fig:dns-message-header}
\end{figure}

After the header section follow the Question, Answer, Authority, and Additional sections. According to the RFC \cite{rfc1035}, the Question section is slightly different from the others. It says that every entry (there is usually only one) contains a domain name, a type, and a class. This triplet could be understood as some kind of key to the specific records.

The other three sections extend this triplet with two other attributes. The first one is the \gls{TTL}. It is a 32-bit unsigned integer that says for how long time (in seconds) can be the records cached. The second attribute is, of course, the data itself. There can be multiple values that are just stacked behind each other and together they create a set of record values.

Formats of these sections are represented in \figref{fig:question-section} (for the Question section) and \figref{fig:aaa-sections} (for Answer, Authority and Additional sections). Notice how do these two formats differ. The beginning is exactly the same, the \figref{fig:aaa-sections} just extends the \figref{fig:question-section} by the \gls{TTL} and the data (length of the data and the data itself), as described above.

Note that the letter \textit{Q} in the \figref{fig:question-section} stands for \textit{question}. The values are still equivalent to values in the \figref{fig:aaa-sections}.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/dns-basics/question-section.ascii}
    \caption{Format of the Question section. \cite{rfc5936}}
    \label{fig:question-section}
\end{figure}

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/dns-basics/aaa-sections.ascii}
    \caption{Format of the Answer, Authority and Additional sections. \cite{rfc5936}}
    \label{fig:aaa-sections}
\end{figure}

At the very end, every message is compressed. This mechanism is supposed to reduce the size of the final message, so the network load is not that high. This mechanism is based on replacing parts of domain names with pointers to a prior occurrence of the same name. So every domain name is written in the message only once and every other use of the same domain name, or its sub-part, is written only as a pointer on the first occurrence with the full wording. Since every domain name can be up to 253 characters long, this mechanism has the potential to save a lot of space.

Message compression is not essential for the purposes of this project, but, as it will be mentioned later, it is important to keep in mind that something like this happens and mainly that it happens at the very end when all the content in all sections is known.

Usually, the \gls{DNS} messages in general are not so long and therefore the \gls{UDP} is used. For example, when resolving an address, questions contain only a desired domain name and answers contain a few records. On the other side, zone transfers usually consist of more records. Tens, hundreds, thousands or even more. That is the reason why the \gls{TCP} is mostly used for zone transfers - for its reliability and stability.

\subsection{AXFR messages} \label{sec:axfr-messages}

The most essential part of zone transfers is the \gls{AXFR} described in RFC 5936 \cite{rfc5936}. This protocol enables to transfer the whole zone at once. It means all records are transferred in one message, or in one set of messages (in case the content is too long for one message, the size limit is 65535 bytes), so the receiver has the current version of the zone.

As explained previously, all \gls{DNS} messages share the same format and differ mostly in the content. Also, messages distinguish whether they are queries (questions) or responses (answers). For this purpose, there is the QR flag in the header of the message.

The \gls{AXFR} query message is sent by a client whenever the zone transfer might be needed and in compliance with the specification the header must contain the following flags:

\begin{itemize}
    \item \textbf{QR} must be set to 0 as an indication that it is a Query.
    \item \textbf{OPCODE} must be set to 0 as an indication that it is a Standard Query.
    \item \textbf{RCODE} must be set to 0 as an indication that there is no error.
    \item \textbf{QDCOUNT} must be set to 1 as an indication that in the Question section there is just 1 entry. This entry describes the question (the triplet of domain name, class, and type).
    \item \textbf{ANCOUNT} must be set to 0 since the Answer section must be empty
    \item \textbf{NSCOUNT} must be set to 0 since the Authority section must be empty
    \item \textbf{ARCOUNT} must be set to the number of records in the Additional section. This section can contain up to 2 records: \gls{EDNS} and \gls{DNS} transaction security. In case of the testing, this section will be mostly empty and therefore the ARCOUNT will be set to 0.
\end{itemize}

More important is then the Question section, which must contain exactly 1 entry - the triplet of domain name, class, and type. The domain name and class are obvious - they contain the key values which the secondary server is asking for. The type is not that obvious since the secondary server does not want just one exact type of records, but all of them. Therefore there is a pseudo \gls{RR} type for this kind of zone transfer, the \gls{AXFR} type with the value of 252.

In case of the response, the header again follows the general specification for \gls{DNS} messages:

\begin{itemize}
    \item \textbf{ID} must be copied from the request.
    \item \textbf{QR} must be set to 1 as an indication that it is a response.
    \item \textbf{OPCODE} must be set to 0 as an indication that it is a Standard Response.
    \item \textbf{AA} must be set to 1 in case there is no error (and therefore RCODE is set to 0). Otherwise, the value must be set according to the rules for that error code.
    \item \textbf{RDCODE} must be set to 0 in case there was no error. Otherwise, this flag should be set to the appropriate value (for example 9 in case the server is not authorized to provide this information or 1 when the query message has been malformed).
    \item \textbf{QDCOUNT} must be set to 0 or 1, further explanation will be provided later.
    \item \textbf{ANCOUNT} must be set to the number of messages sent in the Answer section.
    \item \textbf{NSCOUNT} must be set to 0 since there are no records in the Authority section.
    \item \textbf{ARCOUNT} must be set to the number of records in the Additional section, which has the same rules as for the query message.
\end{itemize}

In the first place, it is important to keep in mind that one \gls{AXFR} response might consist of multiple messages. And the rules for the Question section differ depending on whether or not is the message the first one sent. The Question section must be copied from the query in the first message. In subsequent messages, this section may either remain empty or be copied from the query as well.

The Answer section contains the zone content itself, all the resource records. But the form of this section must comply with few rules. The very first and the very last record must be the \gls{SOA} record of the zone. Thanks to this rule it is possible to identify the last message of the transfer. Not only would the secondary server not know if the connection could be terminated, but there would also be no guarantee that the zone transfer contained all the records and therefore that it was the entire zone.

The resource records between those \gls{SOA} records represent a set, or unordered collection, of records. Although servers usually attempt to send related records as a continuous group, they are not required to do so. However, these records must not contain any \gls{SOA} records since the \gls{SOA} record is already at the beginning and at the end. Violating this rule might result in a zone transfer collapse or, in the worse case, in incorrect processing of the zone transfer and therefore the existence of more different zones versions.

The \gls{AXFR} message is pretty simple, as it can be seen in the example in the \figref{fig:axfr-example}. The zone \textit{nic.cz} has serial number 1 and contains two A records and one MX record.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/dns-basics/axfr-example.ascii}
    \caption{AXFR message example.}
    \label{fig:axfr-example}
\end{figure}

\subsection{IXFR messages} \label{sec:ixfr-messages}

The \gls{IXFR} protocol (described in RFC 1995 \cite{rfc1995}) is an enhancement of the previous \gls{AXFR}. In the \gls{AXFR} messages, plenty of superfluous records are sent since the secondary server might have already had them. In case of one little change (like the addition of one single record), everything has to be transferred again even though the only important part might be just one single record. And that is what this new protocol tries to solve.

The simplest possible description of the \gls{IXFR} is that the message contains only the changes between two zone versions (version is given by the \gls{SOA} serial number).

The header of a query message does not differ from the \gls{AXFR} format. The first difference can be found in the Question section, where the entry does not have the \gls{AXFR} (= 252) type, but \gls{IXFR} type with the value of 251. The domain name and class remain unchanged.

Because the incremental changes represent a difference between two states, an old one and a new one, it is a necessity to have the first state (identified with the \gls{SOA} serial number) from which it is supposed to be started. Therefore in the query message, it is required to have the \gls{SOA} record, from which the incremental changes will be listed. This record is inserted into the Authority section. Note that because there is a record in this section, this information has to be reflected even in the header and therefore the NSCOUNT number must be set to 1.

The server has actually two options for how to answer the query. For some reason, the incremental zone transfer might not be available (for example the client's version is too old and the primary server does not remember all the changes anymore). In this case, the primary server can send the \gls{AXFR} response (it means the whole zone) with one small difference - the Question section is still copied from the query, therefore in the Question section entry the \gls{IXFR} type (= 251) remains unchanged even though it is being responded with the \gls{AXFR}. The fact that the response is not in the form of incremental changes can be ascertained from the form of the Answer section, as described later.

The second, and most preferred, option is to send the incremental changes. These changes between two zone versions can be described as a set of removed records together with a set of added records. Even a change of one particular record (for example change in the value or the \gls{TTL}) can be represented as remove and add operation - just remove the old record and add a new, changed, one.

These two sets of records need to be inserted into the Answer section in a form that the secondary server can distinguish which records are supposed to be added and which of them are supposed to be removed.

As described in the RFC \cite{rfc1995} and as in the \gls{AXFR} response, the very first and the very last record must be the actual \gls{SOA} record of the zone so it is possible to determine the end of the transfer (mostly when it takes more than one message). More important is the content between these \gls{SOA} records.

Firstly, there is the old \gls{SOA} record, which is supposed to be removed. Then follows the set of records which is also supposed to be removed. Next comes the new \gls{SOA} record followed by the set of records to be added.

This one step, this one increment (old \gls{SOA}, removed records, new \gls{SOA}, added records) represents the changes between two versions of a zone. And this increment can be repeated more times in one zone transfer message. So a change from version \textit{n} to version \textit{n+2} can be expressed as either two increments \textit{n $\xrightarrow{}$ n+1 $\xrightarrow{}$ n+2} or one direct compressed increment \textit{n $\xrightarrow{}$ n+2}.

An example with two increments, \textit{1 $\xrightarrow{}$ 2 $\xrightarrow{}$ 3}, is showed in the \figref{fig:ixfr-example-long}. During the changes from 1 to 2, one A record is removed and two are added. During the changes from 2 to 3, one A record is removed (the one which had been added in the previous increment) and another one is added.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/dns-basics/ixfr-example-long.ascii}
    \caption{Example of IXFR message with more increments. \cite{rfc1995}}
    \label{fig:ixfr-example-long}
\end{figure}

This incremental zone transfer can be compressed to a message with just one increment \textit{1 $\xrightarrow{}$ 3}. The very same situation, as in the previous example, is showed in the \figref{fig:ixfr-example-short}, but this time there is just one increment containing one record to remove and two records to add. The record from the previous example, which was added in the version 2 and removed in the version 3, completely disappeared since it was not present in the original version 1 and neither is in the new version 3. The other records really changed between the compared versions and therefore remained in the message.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/dns-basics/ixfr-example-short.ascii}
    \caption{Example of IXFR message with one increment. \cite{rfc1995}}
    \label{fig:ixfr-example-short}
\end{figure}

Notice two important things. The \gls{IXFR} response can be either the \gls{AXFR} or \gls{IXFR}. Also, in the \gls{AXFR} Answer section it is strictly forbidden to have any \gls{SOA} records in the middle (in the meaning of between the border ones) while in the \gls{IXFR} they have the important role of breakpoints.

\subsection{DNS NOTIFY mechanism}

Secondary server can ask his primary server for zone transfer at any time. It can be right after the start, regularly on daily basis, after the \texttt{EXPIRE} countdown from the \gls{SOA} record (this attribute gives the number of seconds after which secondary servers should stop providing the answers for the zone) and so on. But the secondary server itself does not know when is the right time for asking for changes and possibly for the zone transfer.

The primary server is able to notify the secondary server when any change happens. Every change is expressed via increasing the \gls{SOA} serial number and every time this happens, the primary server can send a special message to the secondary server as a notification. The secondary server is not obliged to take this message into consideration, which means that the server is not obligated to ask for changes and the zone transfer immediately. Nevertheless, the secondary server is still obligated to answer this notification as an indication to the primary server that this server knows about the latest happenings.

This mechanism is called A Mechanism for Prompt Notification of Zone Changes (\gls{DNS} NOTIFY) and is specified in the RFC 1996 \cite{rfc1996}. The first message is sent by the primary server which has just made some changes in a zone and therefore increased the \gls{SOA} serial number. This message is characterized by:

\begin{itemize}
    \item Header QR flag is set to 0. It can be understood as a request for keeping up-to-date.
    \item Authoritative Answer (AA) flag should be set to 1 since the primary server is the source of information.
    \item The OPCODE must be set to 4, which is the number reserved for this purpose.
    \item The Question section contains one entry with the domain name, class, and the \gls{SOA} type (= 6).
\end{itemize}

Moreover, this message can also contain the new \gls{SOA} record in the Answer section as an insecure hint for the secondary server. The secondary server is free to use this hint in the meaning of skipping the zone transfer, because it may have been already done using another primary server. However, this hint is optional from the side of the primary server.

As mentioned earlier, the secondary server is still obligated to answer this request. The response is the exactly same message with the only one difference and it is that the QR header flag is set to 1 this time.
