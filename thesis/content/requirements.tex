% SPDX-License-Identifier: MPL-2.0
\chapter{Requirements and expected functionality} \label{chap:implementation-requirements}

It is important to be conscious of what are the goals of the software. It is not a goal, and it is not even possible (as mentioned later), to create software that would test just everything, every possible scenario and every possible problem. The goals are:

\begin{itemize}
    \item to prepare a work-space or environment using which it will be possible to easily and efficaciously implement and run any particular test scenario.
    \item to prepare some basic set of test scenarios covering well-known vulnerabilities.
    \item to make it easy for future use or other programmers to extend the software and add new test cases, since some \gls{DNS} servers might be suffering from product-specific vulnerabilities.
\end{itemize}

Using the \secref{sec:details-of-the-zone-transfer-protocols} it is possible to describe the goal of the software more precisely: The final software should be answering on \gls{XFR} requests with some configured or random content and it should be also capable of checking whether or not have all the responses been correctly processed by the tested secondary server. The secondary server zone version can be obtained again via the \gls{AXFR} protocol. To speed up the testing, the software is also supposed to send \gls{DNS} NOTIFY messages to indicate new zone changes, which should convince the tested secondary server to begin with the zone transfer every time when new changes in a zone are generated.

The software itself must also meet some requirements, which describe its structure and design:

\begin{itemize}
    \item The possibility to reproduce every possible scenario which could ever happen. All the correct scenarios, in the meaning, that the messages could really happen in the real-life operation, and the incorrect scenarios as well, in the meaning, that the messages should be rejected by the tested server.
    \item Make test scenarios implementation as easy as possible. It is not correct if it is needed to write hundreds of lines of code just to create one simple scenario.
    \item The ability to configure independent modules and create a wider range of possible test scenarios. The \gls{DNS} protocol is very complex and also, as described before, there are more ways how to perform a zone transfer. But usually, one way does not differ from another that much. The difference can be just the protocol, which has been used, the content of the \gls{DNS} NOTIFY message, which had been sent before the zone transfer, or an unexpected answer in the form of an AXFR response. All these methods should be somehow configurable so it is easy to combine them in the final scenarios. Said with different words, the components should be as much independent as possible. They should be modular in the way that it should be easy to change or extend them.
    \item The ability of the final software to be extended. The product will be used by programmers and testers (testers in the meaning of \gls{SQA} engineers) and it will help them to ensure that their product, their \gls{DNS} server implementation, is working properly. If they find a bug or a weak spot in their particular product, they might want to create their own test scenario for it so next time the problem is covered by tests. It is required to make this interaction for other programmers simple and elegant.
    \item It is mandatory the software must be easy to run. It is expected that one function takes some configuration parameters and the whole testing process will be executed automatically according to the parameters.
    \item At the end the software will verify the correct processing of all the zone changes by the secondary server. 
    \item The software will collect basic statistics about the network load, which can be then used to compare different implementations.
\end{itemize}

It is important to keep in mind, that the software is supposed to \textbf{be able to reproduce} any scenario, not really reproduce all the scenarios, which could ever happen. It is not even possible since there is an infinite number of combinations (proof: one record can be always added to the zone and so to a zone transfer message). Moreover, the point is not to do a brute force testing but to test the most common, most important, and meaningful scenarios.

Two significant aspects are not very important for the software - time and memory complexity. The software does not need to be really fast or memory efficient. It will not be used as an ordinary program for clients who are waiting for a response. It will be used only for testing purposes, as a utility to ensure the correctness of processing zone transfers. Therefore it is not needed to focus on time or memory complexity, but rather on the abstract and modular design so that the software satisfies all the requirements which have been mentioned so far.

\section{Test example}

For a better understanding of what is supposed to happen, the simplified process of testing is shown in the \figref{fig:workflow-diagram}. Except for the last part, where the final check happens, it is the same as a normal operation in everyday life.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1\linewidth]{figures/requirements/workflow-diagram.pdf}
	\caption{Workflow diagram.}
	\label{fig:workflow-diagram}
\end{figure}

Everything starts on the side of a secondary server. This server knows from its configuration, that there is a zone and that there is also a primary server of this zone. But that is all, at the moment only the information that \textit{something exists} is known. No exact records are known yet. Therefore, the secondary server needs to get these records by sending an \gls{AXFR} request to the primary server. The primary server answers with the zone content and since this moment the secondary server has everything that is needed for serving data. This process is represented in the \textit{Initialization} section in the \figref{fig:workflow-diagram}.

Note that for security reasons the primary server can refuse to provide the \gls{AXFR} response. Usually, secondary servers must be listed in the configuration and \gls{AXFR} requests are refused unless the configured list of secondary servers contains the questioner. The same applies for the \gls{DNS} NOTIFY messages described later. A server follows these messages only if the message came from an authorized source listed in a configuration.

Everything works perfectly until a zone change is made. When this happens, the primary server sends a \gls{DNS} NOTIFY message to the secondary server. The secondary server must confirm the receipt of this notification and then it is supposed to start the zone transfer.

Optionally, a \gls{SOA} request can be sent first to make sure there really are new changes for the particular secondary server. Then, since there is already an old zone version on the side of the secondary server, a request for incremental changes can be sent. Also, for some reason, the \gls{AXFR} protocol can be chosen for the transfer.

In both cases, the primary server is supposed to answer with the corresponding answer. Also, an error can potentially happen. The secondary server has to deal with that cannot make any zone change.

This is how the workflow of \gls{DNS} distributivity really works. Every time a change in the zone occurs, this process is executed all over again.

At the end of the testing, it is needed to check the zone content of the secondary server and that is represented in the \textit{Zone check} part in the \figref{fig:workflow-diagram}. The primary server gets the secondary server's zone via the \gls{AXFR} protocol and compares it with the expected version of the zone.

As explained earlier, the testing software is supposed to test the processing of the zone transfer messages and therefore the main part is generating the responses on \gls{XFR} requests. A simple test scenario can be for example the following:

Create a zone with 10 default A records. Then make 9 changes where one record is removed. Propagate these 9 changes into the secondary server and check that at the end the secondary server's zone really contains just the one and only last record. This scenario is showed in the \figref{fig:test-workflow-example}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1\linewidth]{figures/requirements/test-workflow-example.pdf}
	\caption{Diagram of a test example.}
	\label{fig:test-workflow-example}
\end{figure}

Or a little bit advanced scenario might be to create a zone with 10 default A records, then remove 5 of them and then try to add another 5 new records. But this time answer with a malformed message. It can be just cut in the middle or it can contain incorrect values in the header (for example one bit in the header is reserved and must be 0 every time). After this, check the secondary server's zone version and make sure it contains just the five records. This scenario is showed in the \figref{fig:advanced-test-workflow-example}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=1\linewidth]{figures/requirements/advanced-test-workflow-example.pdf}
	\caption{Diagram of an advanced test example.}
	\label{fig:advanced-test-workflow-example}
\end{figure}

But these scenarios are widely generalized, the exact case can depend even on other details - the \gls{DNS} NOTIFY messages might (not) contain the \gls{SOA} record, it might answer with \gls{AXFR} response on \gls{IXFR} request, it might (not) answer with some malformed messages, used protocol can be one of \gls{TCP} or \gls{UDP}, and so on. The design of the software should make it as simple as possible to adjust all these details and therefore create a much wider variety of test cases.
