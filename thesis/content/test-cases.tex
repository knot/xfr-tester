% SPDX-License-Identifier: MPL-2.0
\chapter{Test cases implementations} \label{chap:test-cases-implementations}

At this point, all the testing environment is designed and all the components have at least one implementation, which can be used for testing. It is the right time for creating the basic set of scenarios to show, how is this testing tool supposed to be used and which can be then used for testing a real server implementation.

\section{Default set of implemented test cases}

The very first, basic and the simplest test scenario is \texttt{BasicARecordsTestCase}. It starts with the serial number 1 and 10 A records in the zone. Then, in every iteration, all records are removed and replaced by another 10 records. The serial number is incremented by 1. The values of the A records have the form \texttt{127.0.X.Y}, where \texttt{X} represents the iteration number (starting with 1 for initial records) and \texttt{Y} is an index of the particular record in each iteration, so values from 1 to 10. Thank to this indexing rule, it is possible to build the whole zone in every situation, the only needed information is an index - the iteration number. All the other data (\gls{TTL} of records and \gls{SOA} refresh, retry, expire, and minimum attributes) are static, so they are not changed during the zone changes. An example of one message with an incremental change between versions with serials 1 and 2 is showed in the \figref{fig:basicarecords-example}.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/test-cases/basicarecords-example.ascii}
    \caption{Example of BasicARecordsTestCase increment change from version 1 to 2.}
    \label{fig:basicarecords-example}
\end{figure}

This scenario is very simple, therefore, unlikely to be inaccurately processed. However, it is also important to test the most obvious scenarios and is advised to have at least one simple scenario. By using this simple test case, it is possible to check that everything is configured correctly and failures of other, more advanced, test cases are not caused because of various setting incongruities.

Next test case, which has been implemented, is the \texttt{SingleRecordTestCase}. In this scenario, a set of records is removed and immediately added back to the zone. So every increment is semantically empty but syntactically nonempty. Moreover, if there are not any other records, the zone is completely empty after applying the \textit{remove} part of the increment. This situation might be forbidden in the configuration of some server implementations, for example BIND9 has \textit{empty-zones-enable} option. But after applying the other part of the increment, the \textit{add} part, the zone becomes non-empty (and therefore valid) again. The secondary server must not fail in the middle of the transfer and must apply the whole increment. Since the zone content looks exactly the same every time (only \gls{SOA} record changes), it is simple to build the whole zone for the final check.

An example of this test case is in the \figref{fig:singlerecord-example}. Increment from the serial 4 to the serial 5 consists of deletion and addition of two same records, A records with values \texttt{127.0.0.1} and \texttt{127.0.0.2}. Note that it is not the same, as the first example. Here the very same records are immediately added back, while in the first case the records have been changed, so the increment was not semantically empty.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/test-cases/singlerecord-example.ascii}
    \caption{Example of SingleRecordTestCase increment change from version 4 to 5.}
    \label{fig:singlerecord-example}
\end{figure}

Another test case is \texttt{MultiIncrementalTestCase}. It tests the capability of processing one zone transfer message consisting of multiple increments, as described in the \secref{sec:ixfr-messages} about \gls{IXFR} messages. This situation is not really common, because usually, the primary servers try to propagate all changes to secondary servers immediately, before any other increment appears, but yet must be covered accordingly to the specification.

Every zone transfer consists of \texttt{N} increments, in every increment one A record and one MX record are removed and one A record and one MX record are also added. Again, the whole zone can be built just by knowing the actual iteration index, so the final zone check can be done easily.

An example is showed in the \figref{fig:multiincrement-example}. The message contains serial changes \textit{1 $\xrightarrow{}$ 2 $\xrightarrow{}$ 3} in one message (so the \texttt{N} parameter equals to 2), every time A and MX records from the previous version are removed and new records are added. The A records are different in the last part of the IP address, the MX records differ in the sub-domain of the exchange attribute.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/test-cases/multiincremental-example.ascii}
    \caption{Example of MultiIncrementalTestCase incremental changes.}
    \label{fig:multiincrement-example}
\end{figure}

Next implemented test case is \texttt{SOAChangesOnlyTestCase}. As the name indicates, the changes in this scenario are only in the \gls{SOA} records. All other records remain untouched. The \gls{IXFR} message contains only 4 \gls{SOA} records in the Answer section, which might seem a little bit strange, but it is a completely valid scenario.

The initial zone contains 3 A records and 2 MX records by default. Then, only \gls{SOA} record is changed, nothing is removed or added, so the final zone consists only from the default records and the actual \gls{SOA} record, which is given by the iteration number (how many iterations have been executed).

Example of this increment is in the \figref{fig:soachangesonly-example}, where the only change is in the \gls{SOA} serial number \textit{5 $\xrightarrow{}$ 6}.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/test-cases/soachangesonly-example.ascii}
    \caption{Example of SOAChangesOnlyTestCase incremental changes.}
    \label{fig:soachangesonly-example}
\end{figure}

Following test case, \texttt{SerialNumberArithmeticTestCase}, is not so simple and it is needed to explain \textit{serial number arithmetic} first. This topic is described in RFC 1982 \cite{rfc1982} in details. Basically, the \gls{SOA} SERIAL number is an unsigned 32-bit integer, which as a data type gives enough space, but not infinite space, so after some finite time, the defined space for serial numbers might overflow.

The solution is to merge the beginning of the scale (\texttt{0x0} in HEX) and the end of the scale (\texttt{0xffffffff}), so \texttt{0xffffffff + 1 = 0x0}. In the mathematical terms, it can be expressed as \texttt{c = (a + b) mod $2^{32}$}. In this way, the scale becomes infinite (or to be more precise - the increments can be done infinitely), but the comparison mechanism must also be adjusted. Reason is obvious - \texttt{0x0} is less than \texttt{0xffffffff}, even though it actually comes right after.

Comparison of two numbers is more complicated. Two numbers are equal if and only if they are the same (if and only if they have the same bites representation). Then, according to the specification \cite{rfc1982}, \texttt{a} is greater than \texttt{b} if and only if \texttt{a} is not equal to \texttt{b} and:
\begin{align}
    \begin{split}
        &(\text{a} < \text{b} \text{ and } \text{b}-\text{a} > 2^{31}) \;\; \text{ or }\\
        &(\text{a} > \text{b} \text{ and } \text{a}-\text{b} < 2^{31})
    \end{split}
\end{align}

That is the exact definition, but it is not really clear how it works. The comparison can be understood in the way, that \texttt{a} is greater than \texttt{b}, if and only if the count of increments from \texttt{b} to \texttt{a} is lower than the count of increments from \texttt{a} to \texttt{b}. It can be visualized via a circle, as shown in the \figref{fig:circle-artithmetic}. Numbers are being incremented clockwise, and the length from \texttt{b} to \texttt{a} is smaller, than the length from \texttt{a} to \texttt{b}. Therefore, serial number \texttt{a} is greater than serial number \texttt{b}.

\begin{figure}[htp]
	\centering
	\includegraphics[width=0.6\linewidth]{figures/test-cases/circle-arithmetic.pdf}
	\caption{Serial number arithmetic visualized with a circle.}
	\label{fig:circle-artithmetic}
\end{figure}

A problem appears when the count of increments from \texttt{a} to \texttt{b} is the same as the count of increments from \texttt{b} to \texttt{a}. In this situation, an attempt to use this ordering operator produces an undefined result.

In the real-life operation it means that serial numbers with their maximum difference of $(2^{32} / 2) - 1 = 2^{31} - 1 = \text{2 147 483 647}$ can be compared while the result still corresponds to reality. If a secondary server is off for a long time, a zone could make up to $2^{31}$ increments and the mechanism would still work perfectly. For illustration, $2^{31}$ seconds is over $68$ years. But the serial number is not required to increment by 1. The increment between zone versions can be higher. One approach for numbering the zone versions is described in the RFC 1912 \cite{rfc1912}. The recommended syntax is \texttt{YYYYMMDDnn}, where \texttt{YYYYMMDD} is the date of the zone change and \texttt{nn} is the revision number of the day (this revision number increments by one and resets to \texttt{01} every day). This format is enough for updating the zone every 15 minutes (what is enough for practical usage) and will not overflow until the year 4294.

\texttt{SerialNumberArithmeticTestCase} is testing exactly this ability to compare serial numbers and refuse zone transfers with serial number, which is not higher than the actual one. Some naive implementations of \gls{DNS} servers may ignore this restriction because it is not very ordinary situation. During the testing, one regular zone transfer is generated (for example $1 \xrightarrow{} \text{2 147 483 648}$, difference $2^{31} - 1$) and after that one false zone transfer is generated ($\text{2 147 483 648} \xrightarrow{} 0$, difference $2^{31}$). The tested secondary server is supposed to refuse the second transfer.

Another implemented test scenario is \texttt{NameOcclusionTestCase}. Name occlusion is a situation, which appears in a combination of \gls{NS} records with other types of records. It is described in the RFC 2136 \cite{rfc2136}. NS records delegate a part of a zone (it could be said \textit{subdomain}) to another server, but there might still be some records in the part of the zone, which has been delegated somewhere else. Those records are fully-fledged, they still belong to the zone, but they cannot be reached during a resolving. For illustration, think of the following example.

A \texttt{com.} server has the following records:

\begin{center}
    \begin{tabular}{ l l l l l }
        example.com.     & 600 & IN & NS & ns.example.com. \\ 
        www.example.com. & 600 & IN & A  & 127.0.0.1   
    \end{tabular}
\end{center}

A resolver is looking for the A record of \texttt{www.example.com}. It asks a root server. The root server answers with an NS record of \texttt{com.} domain. The resolver asks this server with the same question. Now, the \texttt{com.} server knows a desired A record of \texttt{www.example.com.}, but it answers with the \texttt{NS} record, which points to \texttt{ns.example.com.}, so the resolver asks this name server and uses its A record (which can be possibly different). The A record from the \texttt{com.} server with the value \texttt{127.0.0.1} has been somehow ignored, but only during the resolving! This record still belongs to the zone and must be taken into account while zone transfers.

One of the main purposes of \gls{DNS} servers is to provide fast and correct results. Therefore some server implementations might use some kind of efficient structures or algorithms to provide these answers more quickly. But it is still important for zone transfers to keep all the records and work with them (the NS record might get deleted and then the "ignored" A record stops being ignored). And that is exactly what is this test case for.

In the beginning, the zone consists of some NS records, then some static A records (static in the meaning that they are not changed during zone increments), and some dynamic A records (dynamic in the meaning that they are changed during zone increments). One zone change removes the dynamic A records together with the NS records and replaces them with new A and NS records. One change is showed in the \figref{fig:name-occlusion-example}.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/test-cases/name-occlusion-example.ascii}
    \caption{Example of NameOcclusionTestCase incremental changes.}
    \label{fig:name-occlusion-example}
\end{figure}

The last simple test case is named \texttt{JournalTestCase}. Changes of a zone are saved into so-called \textit{journals}. It is useful to have a facility to take a journal and re-run the scenario. And that is exactly what this implementation does. It takes a journal, parses it, and tries to apply all the changes in a specified order. An example of a journal (created by KnotDNS) is illustrated in the \figref{fig:journal-example}. The first section contains the initial zone version with serial number 1, 1 NS record, and 3 A records. Then it contains one incremental change, which is divided into two parts - \textit{removed} and \textit{added}. It is actually a journal of the \texttt{NameOcclusionTestCase} described before.

\begin{figure}[htp]
    \centering
    \inputminted{text}{figures/test-cases/journal-example.ascii}
    \caption{Example of journal for JournalTestCase.}
    \label{fig:journal-example}
\end{figure}

The most important part of this journal test is reading and parsing a journal file. Every row contains either a comment (starts with two semicolons) or one record. Every record consists of 4 parts separated by a tabulator - domain name, \gls{TTL}, record type, and record value. A class of the records is not included in the journal file, so the \texttt{IN} class (= 1) is used by default. Every line containing a resource record is properly parsed and converted to a \texttt{dns.rrset.RRset} instance.

The journal is composed of some sections - the initial zone version and particular zone increments. Each zone increment section consists of two more sub-sections - removed and added records. When reading a journal, it is possible to orient by the comment rows to distinguish between different sections. But this approach is not very stable since the comment formats might differ between products or even versions of one product. A more general approach is to observe the \gls{SOA} records because every single section begins with a \gls{SOA} record and the sections repeat regularly.

The last important piece of information to mention is the size of a journal. A journal can be a few gigabytes big, so it is a bad and dangerous idea to parse the whole journal and then start the testing. A better approach is to read the journal progressively, section by section, and every time when a new section is needed.

The implementation of \texttt{JournalTestCase} takes a reader generator as an argument. The generator is supposed to read a journal and yield every record. The test case then calls the generator every time, when new records are needed and groups the records into desired sections. This design allows to have only one shared test case implementation and provide multiple different journal readers for multiple different journal formats.

Also remind the \texttt{XfrBlackoutTestCase} from the \secref{sec:test-case}. Using this wrapper, it is possible to extend every described test case and create other unique scenarios.

\section{Complex test scenario}

All these simple test cases have one thing in common - they just fill a message with some data. It would be possible to randomly generate some records and insert them into some messages. Possibly, after some time, the randomization might cause that all the previous scenarios would be tested - it might generate a message without changes, just an increment of the \gls{SOA} serial number (alias \texttt{SOAChangesOnlyTestCase}). Or it could generate a message with multiple increments (alias \texttt{MultipleIncrementTestCase}). It could also generate the same combinations of NS records with other types of records (alias \texttt{NameOcclusionTestCase}). Or it could remove a record and immediately add it back into the zone (alias \texttt{SingleRecordTestCase}).

This test case would just use some random records, insert them into some messages and all these cases might be covered, if the test runs for a long enough amount of time. Simple as that. The question is how to solve the randomization and which records should be used.

It should depend on the configuration. If completely everything would be random (random domain names, random record types, random record values, and so on), the space of all possible combinations would be so huge that none of the previously described situations could be possibly generated (actually could, but the probability would be approaching zero). But also, it might be intended to have this randomized test case, so it should be also possible to randomize completely everything.

Test case like this was also designed and implemented. It is named \texttt{ComplexTestCase} and it is based on \textit{records generators}. A record generator is an abstract interface, whose implementations take care of generating the records, which are then inserted into messages, so it is possible to control the randomization aspect, as it was required.

\subsection{Records generators}

A record generator takes care of records, which are then used during the testing. It is a very simple component, which has an abstract interface with two methods - \texttt{next\_add()} and \texttt{next\_remove()}.

The first method, \texttt{next\_add()}, is supposed to generate a record, which is then added into a zone. It returns a \texttt{dns.rrset.RRset} instance, which contains the generated resource record, and this \texttt{RRset} is also inserted into the \gls{XFR} message. Which particular resource record is generated, it depends on the purpose of every implementation. One can focus on generating some records with the same type, another one can focus on generating records of a domain or some implementations can generate just completely random records.

The second method, \texttt{next\_remove()}, takes care of removing the generated items from the zone. It returns either a record (again in the form of a \texttt{RRset} instance) or \texttt{None}. The returned record must have been generated by the \texttt{next\_add()} method first!

Most common implementations would just generate some records in the \texttt{next\_add()} method, save them into a list, and then it would be removing these records from this list one by one in the \texttt{next\_remove()} method. Also, some of the implementations would just randomize the domain name. This general implementation has been already implemented in the \texttt{GenericRecordGenerator} class.

\texttt{GenericRecordGenerator} takes domain and \texttt{randomize\_names} (\texttt{True}/\texttt{False}) flag as arguments in the constructor. Furthermore, it has two class attributes - \texttt{RD\_CLASS} (\texttt{IN} (= 1) by default) and \texttt{RD\_TYPE}. Then, it has a method \texttt{get\_record()}, which has to be implemented in its sub-classes and return a \texttt{dns.rdata.Rdata} instance (which is basically just a single record). Using these, it is possible to take care of the \texttt{next\_add()} and \texttt{next\_remove()} methods, as it is going to be described now.

In the adding part, it just creates an \texttt{RRset} according to the \texttt{RD\_CLASS}, \texttt{RD\_TYPE}, domain and \texttt{randomize\_names} configuration. Then, it inserts one resource record (from the \texttt{get\_record()} method) into this \texttt{RRset} and saves it to a list, which is then used for removing records. Every instance has also a \texttt{self.i} attribute, which means \textit{index}, and this index attribute is incremented every time in the \texttt{next\_add()} method. This index is supposed to be used in the \texttt{get\_record()} method, so it can generate different values every time.

As mentioned, the \texttt{next\_remove()} method just pops first item from the list and returns it. If the list is empty, then \texttt{None} is returned, so no record is removed (because there is actually no record to be removed).

This \texttt{GenericRecordGenerator} makes the implementation of basic record generators very simple. For example a generator of \texttt{A} records just needs to specify the \texttt{RD\_TYPE} attribute and the value of the record in the \texttt{get\_record()} method. It takes just a few lines of code, as it is showed in the \figref{fig:a-generator}.

\begin{figure}[htp]
    \centering
    \inputminted{python}{figures/test-cases/a-generator.py}
    \caption{Implementation of the ARecordsGenerator using GenericRecordGenerator.}
    \label{fig:a-generator}
\end{figure}

Other implemented generators are for \gls{SOA}, \gls{MX} and \gls{NS} records. Also, there is one implementation, which generates random records with unknown types. It means it uses those resource record type values, which are not reserved for any purposes. The value structures of these records are not specified (it cannot be converted into a text format, compressed nor handled in a type-specific way), so the server must treat it as unstructured binary data. Those records must be stored and transmitted without any change \cite{rfc3597}.

\subsection{Complex test case}

The \texttt{ComplexTestCase} basically takes some record generators and uses them to add/remove resource records to/from a zone. The constructor takes a domain, the desired number of iterations, instance of a record generator with \gls{SOA} records, and then a list of generators with normal (in the meaning of not \gls{SOA}) records. Also, it has some optional arguments, which control the count of changes in every iteration, but they are not important for understanding this test case, therefore will not be described.

The initial zone is empty. Every zone is represented as a list of all records. Then, in every iteration, a number of increments is generated and for every increment, there are generated some changes in the zone, which are also saved as the content for \gls{IXFR} messages. These changes are created in the following way:

\begin{itemize}
    \item Generate a random number of attempts to remove a record.
    \item In every attempt, select randomly a record generator and via the \texttt{next\_remove} method get a resource record to remove.
    \item If the return value of the \texttt{next\_remove} method is not \texttt{None} (in most cases it means \textit{nothing to remove}, but of course it can be also a purpose), remove this record from the list of all records and also add this record into the content of the \gls{IXFR} message.
    \item When all attempts are executed, generate a new \gls{SOA} record via the \gls{SOA} records generator (taken in the constructor of the test case) and also add it into the \gls{IXFR} message content.
    \item Generate a random number of attempts to add a record.
    \item For every attempt, select randomly a record generator and via the \texttt{next\_add} method get a resource record to add.
    \item Add this resource record into the list of all records and also add it to the \gls{IXFR} message content.
\end{itemize}

After this loop, the list of all records is updated accordingly to the generated changes and also there is the \gls{IXFR} content, which contains the whole zone update. Everything can be passed to the \texttt{ResponseManager} (described in the \secref{sec:response-manager}) and propagated to a tested secondary server.

Every \texttt{TestCase} must also implement the \texttt{get\_zone} method. The implementation is very simple - the complex test case has the list of all records and furthermore, there is the utility \texttt{get\_zone\_from\_records} (described also in the \secref{sec:test-case-utils}), which can convert this list into the \texttt{dns.zone.Zone} instance.

Shortly - the \texttt{ComplexTestCase} gets a list of record generators and then makes some random changes in the zone. With the right record generators, it is possible to simulate almost all scenarios from the default set and moreover, to create other scenarios very easily.

\section{Testing of KnotDNS}

Software chosen for testing is the \textit{KnotDNS} (\url{https://knot-dns.cz}). It is a high-performance authoritative \gls{DNS} server, which is being developed by a Czech association CZ.NIC and which is deployed (for example) on 3 root servers.

Note that testing of any other \gls{DNS} server implementation (\textit{BIND9}, \textit{PowerDNS}, and others) would be very similar. The main point is that the testing software simulates an authoritative server and acts as an authoritative server. Setup the tested \gls{DNS} server implementation so that it considers the testing software as its primary server of a specified zone and the testing software takes care of the rest.

Testing any software is actually mostly about correct configuration. The tested secondary server and the testing tool (or software) must share their \gls{IP} addresses, ports, and also the zone, which will be used for testing purposes.

For the illustration, the following configuration was used:

\begin{itemize}
    \item \gls{IP} address of the tested secondary (KnotDNS) server is \texttt{127.0.0.1}, port \texttt{5301}.
    \item \gls{IP} address of the primary server is \texttt{127.0.0.1}, port \texttt{5302}.
    \item Used zone is \texttt{dh.}
\end{itemize}

The configuration of the tested KnotDNS server is shown in the \figref{fig:knot-conf}. The first part, \texttt{server}, defines global settings, in this case, the \gls{IP} address, where the server will be running. The \texttt{remote} section defines all the primary servers. Here it is the address, where will be running the testing software. The third section, \texttt{acl}, contains the setting for accepting Notify messages and zone transfers. It also has to be explicitly configured. And finally, in the \texttt{zone} part, the \texttt{dh.} zone is created and the primary server is assigned to it together with the \texttt{acl}.

\begin{figure}[htp]
    \centering
    \inputminted{yaml}{figures/test-cases/knot-conf.yaml}
    \caption{Example configuration of the KnotDNS server.}
    \label{fig:knot-conf}
\end{figure}

In the configuration of the testing tool, the \texttt{Core} (see the \secref{sec:project-core}) takes 3 arguments, which must correspond to the previous settings:

\begin{enumerate}
    \item The zone must be set to \texttt{dh.}, as it is in the configuration of the tested KnotDNS server.
    \item The \texttt{Client} must receive the same \gls{IP} address, which has been set in the \texttt{server} section of the tested KnotDNS server - \texttt{127.0.0.1:5301}
    \item The \texttt{test\_server\_ip} argument must be set to the \texttt{127.0.0.1:5302}, according to the \texttt{remote} section in the KnotDNS configuration file.
\end{enumerate}

The default port of \gls{DNS} servers is \textbf{53} for both \gls{UDP} and \gls{TCP} \cite{rfc1035}, but it is not a requirement. As any other software, even \gls{DNS} servers can listen to any ports they need (according to security options of the machine, where it runs). Therefore, ports and addresses used in the configuration are not set in stone, they can be changed according to any needs. It would be also possible to run the primary server inside one virtual machine and the secondary server in another virtual machine and use the assigned \gls{IP} addresses of those virtual machines together with the ports 53.

When everything is set properly, the testing may begin. The testing tool is run via \texttt{python xfr\_tester}. It takes the scenario configured in the \texttt{\_\_main\_\_.py} file. The KnotDNS server is run with the command \texttt{knotd -c knot.conf} (where the \texttt{knot.conf} is the configuration file, default is \texttt{/etc/knot/knot.conf}). It is also recommended to use the \texttt{--verbose} (\texttt{-v} shortly) flag, which enables debug output. It is very useful during debugging.

When the KnotDNS server is started, it loads the configuration and finds out that there should be a \texttt{dh.} zone. But it does not have any data in the memory, so it automatically starts the zone transfer via \gls{AXFR} (because it has no zone to increment from). This query starts the testing workflow and everything continues automatically.

Every time a zone transfer is executed, Knot sets a timer, which disables initial zone transfers for a period of time. It means that when the server is started repeatedly, the server stops executing the initial zone transfer. It is needed to force the server to perform this initial zone transfer. It can be done using the Knot \gls{DNS} control utility - \textit{knotc}. This utility has the option to refresh all zones via \texttt{knotc -c knot.conf zone-refresh}. This \textit{zone-refresh} starts the initial zone transfer, which then starts the testing workflow again. 

At the end of the testing, the \texttt{xfr\_tester} outputs the results and terminates itself. The KnotDNS server still runs. It is possible to run some custom queries to check the functionality or the server can be also terminated. The server must be restarted before running another test and the zone files must be removed. Otherwise, the server would load the old zone file from the previous test and refuse some zone transfers, because it would evaluate the zone transfers as outdated (its \gls{SOA} record may have a greater serial number).

Note that when running the server with a custom configuration, the server must have permissions to the configuration file. Also, when specifying zone files and journal databases, the server needs the permissions. It is recommended to run the server as the \texttt{knot} user. It can be done using the \texttt{sudo -u knot} command.

KnotDNS has been tested with all described test scenarios, every test case was also extended with the \texttt{XfrBlackoutTestCase} wrapper. All tests were successful, which affirms the reliability of this software.

\subsection{Further testing of KnotDNS} \label{sec:further-testing-of-knot}

According to the testing results, which have been already presented, it seems like the KnotDNS is faultless and perfect. But there is also a possibility, that the testing software (\texttt{xfr\_tester}) is not working properly. It is a great approach to intentionally configure the server incorrectly to see if the tester evaluates the behaviour as incorrect.

Another approach, how to prove (or rather show) that the \texttt{xfr\_tester} is working properly, is to find a real bug and cover it with a test scenario. In this section it is going to be explained, how is the software supposed to be used and it will turn out, how powerful the design really is!

Open-source projects usually maintain something called a \textit{changelog} or \textit{release notes}. It is a very simple text file that contains all the important changes between versions. The KnotDNS also has these release notes (\url{https://gitlab.nic.cz/knot/knot-dns/-/blob/master/NEWS}) and it is possible to find there that one bug was fixed in version 2.7.3: \textit{"Improper processing of an AXFR-style-IXFR response consisting of one-record messages"}! The intention now is clear - install the old, broken version (2.7.2) and try to write a test case, which would detect this bug.

It is going to be useful to be able to easily change KnotDNS versions and try to run the test case with different implementations. Therefore, the \textit{git} repository with all the source code is cloned.

Then, it is intended to install a particular version. Every version is specified via a git \textit{tag} and the installation process is described in the \texttt{README.md} file.

\begin{enumerate}
    \item \texttt{git checkout v2.7.2}
    \item \texttt{autoreconf -it}
    \item \texttt{./configure}
    \item \texttt{make}
    \item \texttt{sudo make install}
    \item \texttt{sudo ldconfig}
\end{enumerate}

The correct installation can be verified by printing the version - \texttt{knotd --version}.

Now, it is needed to write a test case for the situation, which was shortly described in the release notes. It says that it is about \gls{AXFR} answers on \gls{IXFR} queries and that those responses are \textit{"consisting of one-record messages"}.

There are two possible ways how to understand this sentence:

\begin{enumerate}
    \item The content of the \gls{AXFR} message contains only one single resource record, so the whole zone consists only of one record.
    \item One zone transfer message is split into \texttt{N} \gls{DNS} messages, where every message is sent separately and every message contains only one single record in the Answer section.
\end{enumerate}

To test the first case, a new \texttt{TestCase} has to be written. This test case will need a way how to generate the \gls{SOA} records. For this purpose, a \texttt{AbstractRecordGenerator} (which generates \gls{SOA} resource records) from the complex test cases can be used. It saves a lot of work and code! Again - great modular design and reusable components are paying off. Just pass it through the constructor as an argument.

The initial zone is not important and can be empty (so the method has only one line of code). The most important part is generating the changes. Only \gls{AXFR} responses are intended to be used, so it will return an \texttt{AxfrResponseManager}. And this response manager will contain only the \gls{SOA} record and one other record. It can be written on 4 lines of code - update the \gls{SOA}, create a \textit{RRset}, add a record into this \texttt{RRset} and finally return the response manager.

Lastly, the \texttt{get\_zone} method must be implemented. Since the zone really contains just two records, it is also simple. The \texttt{}{dns.zone.Zone} instance can be built directly or using the \texttt{get\_zone\_from\_records} utility. It takes again only 3 lines of code.

After all, the \texttt{require\_zone} method can be overridden to always return \texttt{True}, so the zone check is required in every iteration, and therefore it is possible to watch the server more closely.

The test case is finished and the important parts are only on less than 10 lines of code! Perfectly according to the requirements.

The test case is prepared, KnotDNS is installed with the specified version and the configuration is ready from the previous testing. It is possible to run the scenario. Unfortunately, the test results are successful.

It seems like it was not the bug. But there is still the second possible explanation of the release notes and that is: \textit{"One zone transfer message is split into \texttt{N} \gls{DNS} messages, where every message is sent separately and every message contains only one single record in the answer section."}

It has nothing to do with the changes generation. It is about transforming some data into messages. For this purpose, there is the response manager component! Firstly, clarify the goal:

Every zone change should be propagated into the secondary server via \gls{AXFR} and it should consist of multiple messages where every single message contains only one single resource record in its Answer section.

There already is one implementation of the abstract \texttt{ResponseManager}, which answers only using the \gls{AXFR} - \texttt{AxfrResponseManager}. It is possible to extend it and just add the second feature - the content splitting. In the parent class, the message splitting is performed in the \texttt{\_get\_messages(request, answer)} method, so it is possible just to override this method and adjust the behaviour to the new purpose (the purpose is to create a special message for every resource record). Arguments of this method are the request message and a list of \gls{RRset} instances to be answered.

Every message is supposed to contain only one resource record. It is possible to simplify it - insert only one \texttt{RRset} (not record) into every message and then use a test case, which generates only one record per \texttt{RRset}. For example, the new, previously implemented, test case. The splitting of content is just one simple \textit{for}-loop.

The implementation of the \texttt{ResponseManager} is showed in the \figref{fig:response-manager-implementation}, it is just a few lines of code again. Then, this response manager can be used in the test case and the testing may continue.

This time the test successfully fails! The server output is illustrated in the \figref{fig:knot-fail}. It can be seen that the server tries to process the zone transfer, but it fails to free some memory and is immediately aborted. The connection between the secondary server and the \texttt{xfr\_tester} is unexpectedly closed, which causes an exception, which is then printed on the standard output.

\begin{figure}[htp]
    \centering
    \inputminted{python}{figures/test-cases/response-manager-implementation.py}
    \caption{Implementation of a ResponseManager for special AXFR-style-IXFR answers.}
    \label{fig:response-manager-implementation}
\end{figure}

\begin{figure}[htp]
    \centering
    \inputminted{console}{figures/test-cases/knot-fail.console}
    \caption{The KnotDNS server output, failure.}
    \label{fig:knot-fail}
\end{figure}

Now, upgrade the server to version 2.7.3, which is (according to the release notes) supposed to fix this bug. The newer version can be installed in the very same way, which was described before. The only change is that the checkout must lead to the tag \texttt{v2.7.3}.

After re-installation, run the very same test with the very same configuration. The test finishes successfully, as it can be seen in the log, which is illustrated in the \figref{fig:knot-success}.

\begin{figure}[htp]
    \centering
    \inputminted{console}{figures/test-cases/knot-success.console}
    \caption{The KnotDNS server output, success.}
    \label{fig:knot-success}
\end{figure}

The bug has been corroborated and moreover, a test case has been written. From now on, there is a prepared scenario, which ensures that this bug will not happen again.

This was exactly the way, how is this \texttt{xfr\_tester} supposed to be used. Find a bug or a suspicion and write a test case for it. Creating a new scenario is really simple, as it could be seen, and moreover, thanks to the modular design it is possible to combine the modules and get a larger scale of scenarios.

Note that this bug could have been detected even with the \texttt{ComplexTestCase} with a non-zero probability. According to analysis via Wireshark, the KnotDNS server terminates after receiving the second message. Now imagine the following situation: \texttt{ComplexTestCase} generates some changes, which are too big for one message. Therefore the message must be cut in two. This cutting mechanism is random (described in the \secref{sec:response-manager} about response managers), so with a small (but non-zero) probability, it can be cut right after the first \texttt{RRset}, which is exactly the problematic case.
