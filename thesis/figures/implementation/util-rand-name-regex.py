characters = string.ascii_lowercase + string.digits + '-.'
while True:
    s = ''.join(random.choices(characters, k=252))
    if re.match(r'^([\w\d-]{1,63}\.)+[\w\d-]{1,63}$', s):
        return s
