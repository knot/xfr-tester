while curr_len < max_length:
    label_len = min(
        [max_length - curr_len - 1, rand.randint(1, 63)])
    
    # It's a trap, not enough space for the last label!
    if max_length - curr_len - label_len - 1 == 1:
        # We cannot enlarge the label, just generate another label length
        if label_len == 63:
            continue
        # Or we can enlarge the label length by the missing one byte
        else:
            label_len += 1

    labels.append(rand_label(label_len))
    curr_len += label_len + 1
