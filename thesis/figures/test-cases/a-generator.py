class ARecordsGenerator(GenericRecordGenerator):
    """Generator of A records."""

    RD_TYPE = RdataType.A

    def get_record(self) -> Rdata:
        """Get next A record."""
        return A(RdataClass.IN, RdataType.A, f'127.0.0.{self.i}')
