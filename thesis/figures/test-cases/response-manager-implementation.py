class OneByOneAxfrResponseManager(AxfrResponseManager):
    def _get_messages(
        self, request: Message, answer: List[RRset]
    ) -> List[BytesMessage]:
        ret = []
        for rrset in answer:
            response = make_response(request)
            response.answer.append(rrset)
            ret.append(response)
        return ret
