# SPDX-License-Identifier: MPL-2.0
import inspect
from importlib import import_module
from random import Random
from types import ModuleType

import click
from dns.name import Name, from_text

from xfr_tester.client import TcpClient
from xfr_tester.core import Core
from xfr_tester.datatypes import IPAddress
from xfr_tester.network_analyst import NetworkAnalyst
from xfr_tester.notify_manager import DefaultNotifyManager
from xfr_tester.server import TcpServer
from xfr_tester.test_case.abstract import TestCase

DEFAULT_DOMAIN = 'test.xfr.'
DEFAULT_RANDOM_SEED = None
DEFAULT_CLIENT_IP = '127.0.0.1'
DEFAULT_CLIENT_PORT = 5302
DEFAULT_CLIENT_NA_NAME = 'Client'
DEFAULT_SERVER_PORT = 5301
DEFAULT_SERVER_IP = '127.0.0.1'
DEFAULT_SERVER_NA_NAME = 'Server'
DEFAULT_NOTIFY_SOA_PROB = 0.5


class Domain(click.ParamType):
    """Parameter type for domain names."""

    name = 'Domain'

    def convert(self, value, param, ctx):
        """Convert value into a ``Name`` instance."""
        return from_text(value) if value else None


class Module(click.ParamType):
    """Parameter type for modules."""

    name = 'Module'

    def convert(self, value, param, ctx):
        """Convert path into a module."""
        try:
            return import_module(value)
        except (ImportError, ValueError, TypeError, ModuleNotFoundError):
            raise self.fail(f'Module "{value}" not found.')


class Test(Module):
    """Parameter type for test case instances."""

    name = 'Test path'

    def convert(self, value, param, ctx):
        """Find attribute in module by right splitting by dots."""
        try:
            module_path, method = str(value).rsplit('.', 1)
        except ValueError:
            raise self.fail('Invalid path to a test case instance.')

        module = super().convert(module_path, param, ctx)
        try:
            return getattr(module, method)
        except AttributeError:
            raise self.fail(f'Test case instance {method} not found in {module.__name__}')


@click.command()
@click.help_option('-h', '--help')
@click.version_option('0.0.1')
@click.option('-c', '--config', type=Module(),
              help='Path to a whole config module. It\'s attributes are then passed to the Core as arguments.')
@click.option('-t', '--test-case', type=Test(),
              help='Path to a TestCase instance.')
@click.option('-d', '--domain', default=DEFAULT_DOMAIN, type=Domain(),
              help=f'Domain used for testing. Default: {DEFAULT_DOMAIN}')
@click.option('--random-seed', default=DEFAULT_RANDOM_SEED,
              help=f'Number used as a random seed for default argument values. Default: {DEFAULT_RANDOM_SEED}')
@click.option('--client-ip', default=DEFAULT_CLIENT_IP,
              help=f'IP address of the server under test. Default: {DEFAULT_CLIENT_IP}')
@click.option('--client-port', default=DEFAULT_CLIENT_PORT,
              help=f'Port of the server under test. Default: {DEFAULT_CLIENT_PORT}')
@click.option('--client-na-name', default=DEFAULT_CLIENT_NA_NAME,
              help=f'Title of the default client NetworkAnalyst. Default: {DEFAULT_CLIENT_NA_NAME}')
@click.option('--server-ip', default=DEFAULT_SERVER_IP,
              help=f'IP address of the testing server. Default: {DEFAULT_SERVER_IP}')
@click.option('--server-port', default=DEFAULT_SERVER_PORT,
              help=f'Port of the testing server. Default: {DEFAULT_SERVER_PORT}')
@click.option('--server-na-name', default=DEFAULT_SERVER_NA_NAME,
              help=f'Title of the default server NetworkAnalyst. Default: {DEFAULT_SERVER_NA_NAME}')
@click.option('--notify-soa-prob', default=DEFAULT_NOTIFY_SOA_PROB, type=click.FloatRange(0, 1),
              help=f'Probability of adding a SOA record to the DNS NOTIFY message. Default: {DEFAULT_NOTIFY_SOA_PROB}')
def main(
        config: ModuleType,
        test_case: TestCase,
        domain: Name,
        random_seed: int,
        client_ip: str,
        client_port: int,
        client_na_name: str,
        server_ip: str,
        server_port: int,
        server_na_name: str,
        notify_soa_prob: float,
):
    """
    Test correct processing of zone transfers on a secondary DNS server.

    The configuration of testing can be managed by passing a config (-c, --config) option, which contains path to a
    module, where all the parameters can be adjusted. For a list of all possible arguments and their description, check
    the ``Core`` documentation.

    Sometimes, it is easier to adjust the secondary server configuration to some defaults and then just enter a test
    case instance, not a whole module with other parameters. For this purpose, it is possible to pass only the test case
    (-t, --test-case). This option must contain a path to a TestCase instance.

    Both config and test case instance can be passed at once. The single test case instance has precedence over the
    config, so it is possible to create a config file to use for all tests and then change only the used test case.

    If only a test case instance is passed or the config module does not include all the Core parameters, default
    arguments are used. Some of these default arguments are adjustable via other options (see below), some of them are
    strongly predefined (not adjustable) as the mostly used or optimal values.

    Values used as default:\n
        * Client implementation is the TcpClient class with adjustable address and NetworkAnalyst with all modules.\n
        * Server implementation is the TcpServer class with adjustable address and NetworkAnalyst with all modules.\n
        * NotifyManager implementation is the DefaultNotifyManager with adjustable probability.\n
        * Random instance passed to the DefaultNotifyManager is created with an adjustable seed.\n
    """
    if config is None and test_case is None:
        raise click.BadOptionUsage('config', 'At least one of --test-case or --config is required!')

    # Wrap arguments
    rand = Random(random_seed)
    server_addr = IPAddress(server_ip, server_port)
    client_addr = IPAddress(client_ip, client_port)

    # Get signature parameters
    signature = inspect.signature(Core)
    default_signature = {name: param.default for name, param in signature.parameters.items()}

    # Setup default parameters
    default_main = dict(
        domain=domain,
        client=TcpClient(client_addr, NetworkAnalyst(client_na_name)),
        notify_manager=DefaultNotifyManager(domain, notify_soa_prob, rand=rand),
        server_cls=TcpServer,
        test_server_ip=server_addr,
        server_network_analyst=NetworkAnalyst(server_na_name),
    )

    # Get config and test case parameters
    c_config = {key: getattr(config, key) for key in signature.parameters.keys() if hasattr(config, key)}
    t_config = dict(test_case=test_case) if test_case else {}

    # Merge and check
    final_config = {**default_signature, **default_main, **c_config, **t_config}
    missing = [key for key in final_config.keys() if final_config[key] is inspect._empty]  # type: ignore
    if missing:
        raise click.BadArgumentUsage(f'There are some arguments, which has not been set: {missing}')

    # Run
    core = Core(**final_config)
    core.start()
    core.run()
    core.stop()


if __name__ == '__main__':
    main()
