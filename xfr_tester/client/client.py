# SPDX-License-Identifier: MPL-2.0
from abc import ABC, abstractmethod
from typing import List

from dns.message import Message

from xfr_tester.datatypes import BytesMessage, IPAddress
from xfr_tester.network_analyst import NetworkAnalyst


class Client(ABC):
    """
    Abstract Client class.

    Client is supposed to manage all the connections from the tester to the tested server. Client will be f. e. sending
    the ``DNS NOTIFY`` messages and managing the zone download via AXFR.
    """

    AUTO_ADDR = ('', 0)

    def __init__(self, dest: IPAddress, network_analyst: NetworkAnalyst):
        """
        Client init method.

        Args:
            dest (IPAddress): IP address (in meaning of IP and port) of the tested server.
            network_analyst (NetworkAnalyst): Network analyst instance.
        """
        self.dest = dest
        self.network_analyst = network_analyst

    @abstractmethod
    def send(self, message: BytesMessage) -> Message:
        """
        Send a message to the tested server.

        Args:
             message (BytesMessage): Message to be send.

        Returns:
            Message: The answer from the tested server.
        """
        ...

    @abstractmethod
    def receive_axfr(self, request: BytesMessage) -> List[Message]:
        """
        Manage a zone download via AXFR.

        Args:
            request (BytesMessage): Message to be sent as an AXFR request.

        Returns:
            List[Message]: List of messages received as an answer.
        """
        ...

    def stop(self):
        """Method called after test has been finished to indicate to stop everything."""
        self.network_analyst.print_results()
