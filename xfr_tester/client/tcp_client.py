# SPDX-License-Identifier: MPL-2.0
import socket
from typing import List

from dns.message import Message
from dns.query import receive_tcp, send_tcp

from xfr_tester.datatypes import BytesMessage

from .client import Client


class TcpClient(Client):
    """Client implemented using the TCP protocol."""

    def create_socket(self) -> socket.socket:
        """
        Create socket for communication with the tested server.

        Returns:
            socket.socket: The socked to be used.
        """
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        sock.bind(self.AUTO_ADDR)
        sock.connect(self.dest)
        return sock

    def send(self, message: BytesMessage) -> Message:
        """
        Send a message to the tested server.

        Args:
            message (BytesMessage): Message to be send.

        Returns:
            Message: The answer from the tested server.
        """
        sock = self.create_socket()
        send_tcp(sock, message)
        response, t = receive_tcp(sock)
        self.network_analyst.register_message(response, t)
        sock.close()
        return response

    def receive_axfr(self, request: BytesMessage) -> List[Message]:
        """
        Manage a zone download via AXFR.

        Args:
            request (BytesMessage): Message to be sent as an AXFR request.

        Returns:
            List[Message]: List of messages received as an answer.
        """
        sock = self.create_socket()
        send_tcp(sock, request)
        ret = []

        # First message
        response, t = receive_tcp(sock, one_rr_per_rrset=True)  # one_rr_per_rrset to get the ending SOA record
        self.network_analyst.register_message(response, t)
        first_record = response.answer[0]
        last_record = response.answer[-1]
        ret.append(response)

        # Multi-message response
        while last_record != first_record:
            response, t = receive_tcp(sock, one_rr_per_rrset=True)  # one_rr_per_rrset to get the ending SOA record
            self.network_analyst.register_message(response, t)
            ret.append(response)
            last_record = response.answer[-1]

        sock.close()
        return ret
