# SPDX-License-Identifier: MPL-2.0
from multiprocessing import Event
from multiprocessing.context import Process
from time import sleep
from typing import Callable, List, Optional, Type, Union

from dns.message import Message
from dns.name import Name
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType
from dns.rrset import RRset
from dns.zone import Zone, from_xfr

from xfr_tester.client import Client
from xfr_tester.datatypes import BytesMessage, IPAddress
from xfr_tester.manager import State, TestManager
from xfr_tester.network_analyst import NetworkAnalyst
from xfr_tester.notify_manager import NotifyManager
from xfr_tester.server import Server
from xfr_tester.test_case.abstract import TestCase


class TestException(Exception):
    """Test exception."""


class Core:
    """Core of the testing."""

    def __init__(
            self,
            domain: Name,
            client: Client,
            test_case: TestCase,
            notify_manager: NotifyManager,
            server_cls: Type[Server],
            test_server_ip: IPAddress,
            server_network_analyst: NetworkAnalyst,
            iter_wait: Union[bool, Callable[[], bool]] = True,
            iter_sleep: Union[None, float, Callable[[], float]] = None,
            compare_zone_attempts: int = 3,
            compare_zone_sleep_time: float = 0.5,
    ):
        """
        Init method.

        Args:
            domain (Name): Domain used for test purposes.
            client (Client): Client instance.
            test_case (TestCase): Test case instance.
            notify_manager (NotifyManager): Notify manager instance.
            server_cls (Type[Server]): Server class to be used as the server.
            test_server_ip (IPAddress): IP address (in meaning of the tuple IP and port) to be used as the test server
                                        address.
            server_network_analyst (NetworkAnalyst): Network analyst instance to be used as a server analyst.
            iter_wait (Union[bool, Callable[[], bool]]): Indication whether or not should we wait for the tested server.
            iter_sleep (Union[None, float, Callable[[], float]]): For how long time should we sleep in the iteration.
            compare_zone_attempts (int): How many times should we try to compare the zones before failing.
            compare_zone_sleep_time (int): For how long time should we wait after a zone check failure.
        """
        self.domain = domain
        self.client = client
        self.test_case = test_case
        self.notify_manager = notify_manager
        self.server_cls = server_cls
        self.test_server_ip = test_server_ip
        self.server_network_analyst = server_network_analyst
        self.iter_wait = iter_wait
        self.iter_sleep = iter_sleep
        self.compare_zone_attempts = compare_zone_attempts
        self.compare_zone_sleep_time = compare_zone_sleep_time

        self.manager = TestManager()
        self.server_process: Optional[Process] = None
        self.state = None  # type: Optional[State]
        self.event = Event()

    def start(self) -> None:
        """Start all modules, prepare for testing."""
        self.manager.start()
        self._start_server()

    def stop(self) -> None:
        """Stop all modules after testing."""
        assert self.server_process is not None  # Process must be started
        self.server_process.terminate()
        self.server_process.join()
        self.manager.shutdown()
        self.client.stop()

    def _start_server(self) -> None:
        """Start the server."""
        self.state = self.manager.State(self.test_case.get_init_data())  # type: ignore

        self.server_process = Process(
            name='Server',
            target=self.server_cls.start_run,
            args=(self.test_server_ip, self.state, self.event, self.server_network_analyst),
        )
        self.server_process.start()

    def _get_iter_sleep_time(self) -> float:
        """Get the sleep time in the iteration."""
        if isinstance(self.iter_sleep, float):
            return self.iter_sleep
        if callable(self.iter_sleep):
            return self.iter_sleep()
        return 0.0

    def _get_iter_wait(self) -> bool:
        """Get the boolean whether or not to wait in the iteration."""
        if callable(self.iter_wait):
            return self.iter_wait()
        return self.iter_wait

    def run(self) -> None:
        """Run the tests."""
        assert self.state is not None  # Core has been started

        # Wait for initial data download
        self.event.wait()

        # Run tests
        for response_manager in self.test_case:
            # Set new state
            self.state.set_response_manager(response_manager)

            # Notify the secondary server about changes
            self.event.clear()
            _ = self.client.send(self.notify_manager.get_notify_message(response_manager.get_soa_rr()))

            # Wait until the server asks for the changes
            if self._get_iter_wait():
                self.event.wait()

            # Give the server some time to apply the changes
            if self.iter_sleep is not None:
                sleep(self._get_iter_sleep_time())

            # If required, compare actual zones
            if self.test_case.require_zone():
                if not self.compare_zones(self.test_case.get_zone()):
                    break
        else:
            # If everything went well, make the final zone comparison
            self.compare_zones(self.test_case.get_zone())

    def compare_zones(self, expected_zone: Zone) -> bool:
        """
        Compare zones between test and tested servers.

        Args:
            expected_zone (Zone): Zone of the test server.

        Returns:
            bool: Whether or not the zones are the same.
        """
        error = None
        for _ in range(self.compare_zone_attempts):
            axfr = self.client.receive_axfr(self.get_axfr_request())
            try:
                self.compare_results(axfr, expected_zone)
            except TestException as e:
                error = e
                sleep(self.compare_zone_sleep_time)
            else:
                print('Test has been successful!')
                return True
        else:
            print(error)
            return False

    @staticmethod
    def compare_results(axfr_response: List[Message], expected_zone: Zone) -> None:
        """
        Compare results between tested server AXFR response and expected test zone.

        Args:
            axfr_response (List[Message]): Received response messages from the AXFR request.
            expected_zone (Zone): Zone which should have been received from the tested server as actual state.
        """
        received_zone = from_xfr(axfr_response, relativize=False, check_origin=False)

        # Basic check
        if received_zone.rdclass != expected_zone.rdclass:
            raise TestException('Incorrect rdclass.')
        if received_zone.origin != expected_zone.origin:
            raise TestException('Incorrect origin.')

        # Check that we have the same count of nodes to be able to compare them
        if len(received_zone.nodes) != len(expected_zone.nodes):
            raise TestException('Zones do not have the same count of nodes.')

        # Compare first zone nodes with the second zone nodes
        for name, node in expected_zone.nodes.items():
            nd_node = received_zone.nodes.get(name)
            if not nd_node:
                raise TestException(f'A node "{name}" has not been found.')

            # Check rdatasets length equality
            if len(node.rdatasets) != len(nd_node.rdatasets):
                raise TestException(f'Node "{name}" does not have correct count of rdatasets.')

            # Check the datasets
            for rdataset in node.rdatasets:
                nd_rdataset = nd_node.get_rdataset(rdataset.rdclass, rdataset.rdtype)
                if not nd_rdataset:
                    raise TestException(f'A rdataset has not been found in node "{name}".')
                if rdataset.ttl != nd_rdataset.ttl:
                    raise TestException(f'Received TTL does not match in node "{name}".')
                if rdataset != nd_rdataset:
                    raise TestException(f'There is an incorrect rdataset in node "{name}".')

    def get_axfr_request(self) -> BytesMessage:
        """Get AXFR request message."""
        msg = Message()
        rr = RRset(self.domain, RdataClass.IN, RdataType.AXFR)
        msg.question.append(rr)
        return msg

    @classmethod
    def test(cls, *args, **kwargs):
        """Shortcut for running a start-run-stop."""
        core = cls(*args, **kwargs)
        core.start()
        core.run()
        core.stop()
