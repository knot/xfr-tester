# SPDX-License-Identifier: MPL-2.0
from typing import List, NamedTuple, Union

from dns.message import Message

IPAddress = NamedTuple('IPAddress', (('addr', str), ('port', int)))
BytesMessage = Union[Message, bytes]
ResponseManagerResult = NamedTuple('ResponseManagerResult', (('responses', List[BytesMessage]), ('force_close', bool)))
