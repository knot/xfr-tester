# SPDX-License-Identifier: MPL-2.0
from multiprocessing.managers import BaseManager

from dns.message import Message

from xfr_tester.datatypes import ResponseManagerResult
from xfr_tester.test_case.abstract import ResponseManager


class State:
    """Shared state between processes (tester and server)."""

    def __init__(self, response_manager: ResponseManager):
        """
        Init method.

        Args:
            response_manager (ResponseManager): Shared response manager.
        """
        self.response_manager = response_manager

    def get_responses(self, request: Message) -> ResponseManagerResult:
        """Get response messages for the request."""
        return self.response_manager.get_responses(request)

    def set_response_manager(self, response_manager: ResponseManager) -> None:
        """Change the ``ResponseManager``."""
        self.response_manager = response_manager


class TestManager(BaseManager):
    """Manager for testing purposes, to be shared between processes."""


TestManager.register('State', State)
