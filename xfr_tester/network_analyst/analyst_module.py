# SPDX-License-Identifier: MPL-2.0
from abc import ABC, abstractmethod
from collections import defaultdict
from typing import Union

from dns.message import Message
from dns.rdatatype import RdataType


class AnalystModule(ABC):
    """Abstract analyst module."""

    MODULE_DESCRIPTION: str = ''

    def __init__(self):
        """Init method."""
        self.stat_unit: Union[float, int] = 0

    def get_module_description(self) -> str:
        """Get module description."""
        return self.MODULE_DESCRIPTION or self.__class__.__name__

    @abstractmethod
    def register_message(self, message: Message, time_received: float) -> None:
        """Register received message for statistics."""

    def get_results(self) -> str:
        """Get results of the statistics."""
        return str(self.stat_unit)


class FirstMessageTimeModule(AnalystModule):
    """Module for counting received messages."""

    MODULE_DESCRIPTION = 'First message received'

    def register_message(self, message: Message, time_received: float) -> None:
        """Register message and compare times."""
        self.stat_unit = self.stat_unit or time_received


class LastMessageTimeModule(AnalystModule):
    """Module for counting received messages."""

    MODULE_DESCRIPTION = 'Last message received'

    def register_message(self, message: Message, time_received: float) -> None:
        """Register message and save new last-message time."""
        self.stat_unit = time_received  # Method is called chronologically, no need to use max()


class MessagesCounterModule(AnalystModule):
    """Module for counting received messages."""

    MODULE_DESCRIPTION = 'Total messages received'

    def register_message(self, message: Message, time_received: float) -> None:
        """Register one new message."""
        self.stat_unit += 1


class BytesCounterModule(AnalystModule):
    """Module for counting received messages."""

    MODULE_DESCRIPTION = 'Total bytes received'

    def register_message(self, message: Message, time_received: float) -> None:
        """Register new message and count bytes."""
        self.stat_unit += len(message.to_wire())


class MessageTypesCounterModule(AnalystModule):
    """Module for counting RD types in question section of the message."""

    MODULE_DESCRIPTION = 'Message types'

    def __init__(self):
        """Init method."""
        super().__init__()
        self.stat_units = defaultdict(lambda: 0)

    def register_message(self, message: Message, time_received: float) -> None:
        """Record message type."""
        rdtype = RdataType(message.question[0].rdtype)
        self.stat_units[rdtype] += 1

    def get_results(self) -> str:
        """Get results of the statistics in the form of a normal dictionary."""
        return str({k.name: v for k, v in self.stat_units.items()})


ALL_MODULES = [
    FirstMessageTimeModule(),
    LastMessageTimeModule(),
    MessagesCounterModule(),
    BytesCounterModule(),
    MessageTypesCounterModule(),
]
