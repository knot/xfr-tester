# SPDX-License-Identifier: MPL-2.0
from typing import Iterable

from dns.message import Message

from .analyst_module import ALL_MODULES, AnalystModule


class NetworkAnalyst:
    """Analyst of network load."""

    def __init__(self, title: str, modules: Iterable[AnalystModule] = None, description_length: int = 25):
        """
        Init method.

        Args:
            title (str): Title of the analyst.
            modules (Iterable[AnalystModule]): List of analyst modules, which do all the metrics. Default all modules.
            description_length (int): Length of the first column on results printing.
        """
        self.title = title
        self.modules = modules or ALL_MODULES
        self.description_length = description_length

    def register_message(self, message: Message, time_received: float) -> None:
        """Apply message registration on every module."""
        for module in self.modules:
            module.register_message(message, time_received)

    def print_results(self) -> None:
        """Print results from all modules."""
        print(f'=== Network analyst - {self.title} ===')
        for module in self.modules:
            mdl = len(module.get_module_description()) + 1
            spaces = max(0, self.description_length - mdl)
            print(f'{module.get_module_description()}:{" " * spaces} {module.get_results()}')
