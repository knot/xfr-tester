# SPDX-License-Identifier: MPL-2.0
from random import Random

from dns import opcode
from dns.flags import Flag
from dns.message import Message
from dns.name import Name
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType
from dns.rrset import RRset

from xfr_tester.datatypes import BytesMessage

from .notify_manager import NotifyManager


class DefaultNotifyManager(NotifyManager):
    """Default Notify manager with the ability to set whether or not will be the ``SOA`` hint sent."""

    def __init__(self, domain: Name, add_soa_prob: float = 0, rand: Random = None):
        """Init method."""
        super().__init__(domain)
        self.add_soa_prob = add_soa_prob
        self.rand = rand or Random()

    def get_notify_message(self, soa_rr: RRset) -> BytesMessage:
        """
        Return a message, which will be send to notify the tested server about some new changes in the tested zone.

        Args:
            soa_rr (RRset): RRset with the SOA record of the zone.

        Returns:
            BytesMessage: Message to be sent as the ``DNS NOTIFY`` message.
        """
        assert soa_rr.rdtype == RdataType.SOA, 'Expected SOA record type.'
        assert soa_rr.rdclass == RdataClass.IN, 'Expected SOA record class.'
        assert len(soa_rr) == 1, 'SOA RRset must contain exactly one record.'
        soa_f_clear = RRset(self.domain, RdataClass.IN, RdataType.SOA)

        msg = Message()
        msg.flags |= Flag.AA
        msg.flags |= opcode.to_flags(opcode.Opcode.NOTIFY)
        msg.question.append(soa_f_clear)

        if self.add_soa_prob != 0:
            if self.add_soa_prob == 1 or self.add_soa_prob > self.rand.random():
                msg.answer.append(soa_rr)

        return msg
