# SPDX-License-Identifier: MPL-2.0
from abc import ABC, abstractmethod

from dns.name import Name
from dns.rrset import RRset

from xfr_tester.datatypes import BytesMessage


class NotifyManager(ABC):
    """
    Abstract Notify Manager.

    Class for generating the ``DNS NOTIFY`` messages.
    """

    def __init__(self, domain: Name):
        """
        Notify manager init method.

        Args:
            domain (Name): The domain which is will be managed.
        """
        self.domain = domain

    @abstractmethod
    def get_notify_message(self, soa_rr: RRset) -> BytesMessage:
        """
        Return a message, which will be send to notify the tested server about some new changes in the tested zone.

        Args:
            soa_rr (RRset): RRset with the SOA record of the zone.

        Returns:
            BytesMessage: Message to be sent as the ``DNS NOTIFY`` message.
        """
