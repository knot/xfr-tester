# SPDX-License-Identifier: MPL-2.0
import signal
from abc import ABC, abstractmethod
from multiprocessing.synchronize import Event

from xfr_tester.datatypes import IPAddress
from xfr_tester.manager import State
from xfr_tester.network_analyst import NetworkAnalyst


class SigtermException(Exception):
    """Signalize SIGTERM to close the server socket and stop the server."""


class Server(ABC):
    """
    Abstract server class.

    Server is supposed to run in the background and respond to all questions from the tested server. For generating all
    the answers there is a shared ``state``, where the answers are generated.
    """

    def __init__(self, address: IPAddress, state: State, event: Event, network_analyst: NetworkAnalyst):
        """
        Init method.

        Args:
            address (IPAddress): IP address (in meaning of IP address and port), where the server is supposed to run.
            state (State): Shared state for generating responses.
            event (Event): Multiprocessing feature for processes synchronization.
            network_analyst (NetworkAnalyst): Network analyst instance.
        """
        self.address = address
        self.state = state
        self.event = event
        self.network_analyst = network_analyst
        signal.signal(signal.SIGTERM, self.handle_sigterm)

    @classmethod
    def start_run(cls, *args, **kwargs) -> None:
        """Create Server instance, start, run and stop."""
        s = cls(*args, **kwargs)
        s.start()
        s.run()
        s.stop()

    def handle_sigterm(self, signum: int, frame) -> None:
        """Handle the sigterm."""
        raise SigtermException()

    @abstractmethod
    def start(self) -> None:
        """Start the ``Server``."""
        ...

    @abstractmethod
    def run(self) -> None:
        """Run the server - answer on requests."""
        ...

    @abstractmethod
    def stop(self) -> None:
        """Stop the ``Server``."""
        ...
