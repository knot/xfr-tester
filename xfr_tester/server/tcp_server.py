# SPDX-License-Identifier: MPL-2.0
import socket
from typing import Optional

from dns.query import receive_tcp, send_tcp

from .server import Server, SigtermException


class TcpServer(Server):
    """Server implemented using the TCP protocol."""

    def __init__(self, *args, **kwargs):
        """Init method of the ``Server``."""
        super().__init__(*args, **kwargs)
        self.sock: Optional[socket.socket] = None

    def start(self) -> None:
        """
        Start the ``Server``.

        Create socket, bind an address and listen.
        """
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        self.sock.bind(self.address)
        self.sock.listen(5)

    def run(self) -> None:
        """Run the server - answer on requests."""
        assert self.sock is not None  # Server must be started
        try:
            while True:
                sock, _ = self.sock.accept()
                try:
                    while True:
                        message, t = receive_tcp(sock)
                        self.network_analyst.register_message(message, t)
                        responses, force_kill = self.state.get_responses(message)
                        for response in responses:
                            send_tcp(sock, response)
                        if force_kill:
                            break
                except EOFError:
                    pass
                sock.close()
                self.event.set()
        except SigtermException:
            pass

    def stop(self) -> None:
        """
        Stop the ``Server``.

        Close sockets and print network results.
        """
        assert self.sock is not None  # Server must be started
        self.sock.close()
        self.network_analyst.print_results()
