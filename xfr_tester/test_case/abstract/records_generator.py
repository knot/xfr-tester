# SPDX-License-Identifier: MPL-2.0
from abc import ABC, abstractmethod
from random import Random
from typing import List, Optional

from dns.name import Name
from dns.rdata import Rdata
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType
from dns.rrset import RRset

from xfr_tester.utils import rand_name, rand_ttl


class AbstractRecordGenerator(ABC):
    """Abstract record generator."""

    def __init__(self, domain: Name, rand: Random = None):
        """
        Init method of the records generator.

        Args:
            domain (Name): From which domain should be the records generated.
            rand: (Random): Random instance for test replication.
        """
        self.domain = domain
        self.rand = rand or Random()
        self.records: List[RRset] = []
        self.i = 0

    @abstractmethod
    def next_add(self) -> RRset:
        """Get next record to add to the zone."""

    @abstractmethod
    def next_remove(self) -> Optional[RRset]:
        """
        Get next record to remove from the zone.

        The generator is supposed to check whether or not the record has been already generated. If there are no records
        to be removed, return ``None``.
        """


class GenericRecordGenerator(AbstractRecordGenerator, ABC):
    """Abstract class for basic records generators."""

    RD_CLASS: RdataClass = RdataClass.IN
    RD_TYPE: RdataType

    def __init__(self, randomize_names: bool = True, *args, **kwargs):
        """
        Init method.

        Args:
            randomize_names (bool): Whether or not should be the names randomized (create random subdomains).
        """
        super().__init__(*args, **kwargs)
        self._ttl = rand_ttl(self.rand)
        self._randomize_names = randomize_names

    def get_rrset(self) -> RRset:
        """Get ``RRset`` for the actual class and type of records."""
        assert getattr(self, 'RD_TYPE', None), 'Expected a RD_TYPE attribute.'
        domain = rand_name(origin=self.domain, rand=self.rand) if self._randomize_names else self.domain
        return RRset(domain, self.RD_CLASS, self.RD_TYPE)

    def get_record(self) -> Rdata:
        """Get a record to be added."""
        raise NotImplementedError('get_record() method must be implemented in order to use this feature.')

    def next_add(self) -> RRset:
        """Get an ``RRset`` to be added into the zone."""
        self.i += 1
        self._ttl = rand_ttl(self.rand) if not self.records else self._ttl
        rr = self.get_rrset()
        rr.add(self.get_record(), self._ttl)
        self.records.append(rr)
        return rr

    def next_remove(self) -> Optional[RRset]:
        """Get next ``RRset`` to be removed or ``None`` when there is no record to be removed."""
        try:
            return self.records.pop(0)
        except IndexError:
            return None
