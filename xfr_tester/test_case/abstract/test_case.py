# SPDX-License-Identifier: MPL-2.0
from abc import ABC, abstractmethod

from dns.message import Message
from dns.rrset import RRset
from dns.zone import Zone

from xfr_tester.datatypes import ResponseManagerResult


class ResponseManager(ABC):
    """Response manager which manages responses for answers."""

    @abstractmethod
    def get_responses(self, request: Message) -> ResponseManagerResult:
        """
        Return responses for the request.

        Args:
            request (Message): A request, a question to be answered.

        Returns:
            ResponseManagerResult: List of messages to be send as the response together with a bool flag whether or not
            cancel the connection immediately.
        """

    @abstractmethod
    def get_soa_rr(self) -> RRset:
        """Get ``RRset`` with the SOA record."""


class TestCase(ABC):
    """Test case, test scenario."""

    def __iter__(self):
        """Return a generator for testing."""
        return self

    @abstractmethod
    def __next__(self) -> ResponseManager:
        """Generate next zone changes and therefore the new ``ResponseManager``."""

    @abstractmethod
    def get_init_data(self) -> ResponseManager:
        """Get initial data for testing in the form of a ``ResponseManager``."""

    @abstractmethod
    def get_zone(self) -> Zone:
        """Return actual zone."""

    def require_zone(self) -> bool:
        """Require a zone somewhere in the middle of testing, at an interesting point of test iteration."""
        return False
