# SPDX-License-Identifier: MPL-2.0
from collections import defaultdict
from functools import partial
from typing import Dict, Optional

from dns.name import Name, from_text
from dns.rdata import GenericRdata, Rdata
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType
from dns.rdtypes.ANY.MX import MX
from dns.rdtypes.ANY.NS import NS
from dns.rdtypes.ANY.SOA import SOA
from dns.rdtypes.IN.A import A
from dns.rrset import RRset

from xfr_tester.test_case.abstract import AbstractRecordGenerator, GenericRecordGenerator
from xfr_tester.utils import rand_name, rand_ttl


class MaxDiffSoaRecordGenerator(GenericRecordGenerator):
    """Generator for SOA records with the biggest possible step between serial numbers."""

    RD_TYPE = RdataType.SOA

    def __init__(self, serial_bits: int = 32, *args, **kwargs):
        """
        Init method.

        Args:
            serial_bits (int): Number of bits to be used to compute the serial number.
        """
        super().__init__(*args, **kwargs)
        self.serial_bits = serial_bits
        self.serial = 1

    def get_rrset(self) -> RRset:
        """Get empty SOA ``RRset``."""
        return RRset(self.domain, self.RD_CLASS, self.RD_TYPE)

    def get_record(self) -> SOA:
        """Get another SOA record."""
        self.i += 1
        self.serial = (self.serial + 2 ** (self.serial_bits - 1) - 1) % (2 ** self.serial_bits)
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text('admin', self.domain), self.serial,
                   *[rand_ttl(self.rand) for _ in range(4)])


class OneByOneSoaRecordGenerator(GenericRecordGenerator):
    """Generator for SOA records with only one step between serial numbers."""

    RD_TYPE = RdataType.SOA

    def get_rrset(self) -> RRset:
        """Get empty SOA ``RRset``."""
        return RRset(self.domain, self.RD_CLASS, self.RD_TYPE)

    def get_record(self) -> Rdata:
        """Get another SOA record."""
        self.records = []  # We do not need to save all records
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text('admin', self.domain), self.i,
                   *[rand_ttl(self.rand) for _ in range(4)])


class ARecordsGenerator(GenericRecordGenerator):
    """Generator of A records."""

    RD_TYPE = RdataType.A

    def get_record(self) -> Rdata:
        """Get next A record."""
        params = [(self.i // (256 ** p)) % 256 for p in range(2, -1, -1)]
        return A(RdataClass.IN, RdataType.A, '127.{}.{}.{}'.format(*params))


class MXRecordsGenerator(GenericRecordGenerator):
    """Generator of MX records."""

    RD_TYPE = RdataType.MX

    def __init__(self, preference: int = 1000, subdomain: str = 'mail', *args, **kwargs):
        """
        Init method.

        Args:
            preference (int): Preference of the MX records.
            subdomain (str): Subdomain name to be prepended.
        """
        super().__init__(*args, **kwargs)
        self.preference = preference
        self.subdomain = subdomain

    def get_record(self) -> Rdata:
        """Get next MX record."""
        domain = Name((f'{self.subdomain}{self.i}',)).concatenate(self.domain)
        return MX(self.RD_CLASS, self.RD_TYPE, self.preference, domain)


class NSRecordsGenerator(GenericRecordGenerator):
    """Generator of NS records."""

    RD_TYPE = RdataType.NS

    def __init__(self, target: str = 'ns', *args, **kwargs):
        """
        Init method.

        Args:
            target (str): Subdomain used as the target.
        """
        super().__init__(*args, **kwargs)
        self.target = target

    def get_record(self) -> Rdata:
        """Get next NS record."""
        return NS(self.RD_CLASS, self.RD_TYPE, Name((f'{self.target}{self.i}',)).concatenate(self.domain))


class UnknownRecordsGenerator(AbstractRecordGenerator):
    """Generator of records with unknown type."""

    RD_CLASS: RdataClass = RdataClass.IN

    def __init__(self, randomize_names: bool = True, *args, **kwargs):
        """
        Init method.

        Args:
            randomize_names (bool): Whether or not should be randomized subdomain prepended.
        """
        super().__init__(*args, **kwargs)
        self._randomize_names = randomize_names

        # Generator memory - we need to balance TTLs across all possible records with the same rdtype.
        #
        # If no active records available, then zero is count of active records available.
        # If no TTL for the rdtype is available, then assign a random TTL to the rdtype.
        self._active_records: Dict[int, int] = defaultdict(lambda: 0)
        self._ttls: Dict[int, int] = defaultdict(partial(rand_ttl, self.rand))

    def next_add(self) -> RRset:
        """Get next record to be added into the zone."""
        # Generate RR type
        while True:
            rdtype = self.rand.randint(0, RdataType._maximum())
            try:
                RdataType(rdtype)
            except ValueError:
                break  # Found an unknown type
            else:
                continue  # This type is well-known

        # Prepare data
        domain = rand_name(origin=self.domain, rand=self.rand) if self._randomize_names else self.domain
        ttl = self._ttls[rdtype]
        self._active_records[rdtype] += 1
        data = f'rdclass={self.RD_CLASS};rdtype={rdtype};ttl={ttl};random={rand_ttl(self.rand)}'.encode('utf-8')

        # Put data into structures
        rrset = RRset(domain, self.RD_CLASS, rdtype)
        rrset.add(GenericRdata(self.RD_CLASS, rdtype, data), ttl)
        self.records.append(rrset)

        return rrset

    def next_remove(self) -> Optional[RRset]:
        """Get a record to remove from the zone, if any."""
        # Find a record to remove
        try:
            rrset = self.records.pop(0)
        except IndexError:
            return None

        # Manage generator memory
        self._active_records[rrset.rdtype] -= 1
        if self._active_records[rrset.rdtype] == 0:
            del self._active_records[rrset.rdtype]
            del self._ttls[rrset.rdtype]
        return rrset
