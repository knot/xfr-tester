# SPDX-License-Identifier: MPL-2.0
from random import Random
from typing import List

from dns.name import Name
from dns.rrset import RRset
from dns.zone import Zone

from xfr_tester.test_case.abstract import AbstractRecordGenerator, ResponseManager, TestCase
from xfr_tester.test_case.response_manager import AxfrResponseManager, DummyResponseManager
from xfr_tester.utils import get_zone_from_records


class ComplexTestCase(TestCase):
    """Complex test case."""

    def __init__(
            self,
            domain: Name,
            iterations: int,
            soa_generator: AbstractRecordGenerator,
            record_generators: List[AbstractRecordGenerator],
            *,
            rand: Random = None,
            min_increments_per_step: int = 1,
            max_increments_per_step: int = 5,
            min_remove_records_per_increment: int = 1,
            max_remove_records_per_increment: int = 5,
            min_new_records_per_increment: int = 1,
            max_new_records_per_increment: int = 5
    ):
        """
        Init method.

        Args:
            domain (Name): Domain, which is being used for testing purposes.
            iterations (int): Number of iterations.
            soa_generator (AbstractRecordGenerator): Generator used for SOA records.
            record_generators (List[AbstractRecordGenerator]): List of record generators for the zone records.
            rand (Random): Random instance for test replication.
            min_increments_per_step (int): Minimal count of zone increments (serial increments) for one iteration.
            max_increments_per_step (int): Maximal count of zone increments (serial increments) for one iteration.
            min_remove_records_per_increment (int): Minimal count of records to be removed in every step.
            max_remove_records_per_increment (int): Maximal count of records to be removed in every step.
            min_new_records_per_increment (int): Minimal count of records to be added in every step.
            max_new_records_per_increment (int): Maximal count of records to be added in every step.
        """
        self.domain = domain
        self.iterations = iterations
        self.soa_generator = soa_generator
        self.record_generators = record_generators
        self.rand = rand or Random()
        self.min_increments_per_step = min_increments_per_step
        self.max_increments_per_step = max_increments_per_step
        self.min_remove_records_per_increment = min_remove_records_per_increment
        self.max_remove_records_per_increment = max_remove_records_per_increment
        self.min_new_records_per_increment = min_new_records_per_increment
        self.max_new_records_per_increment = max_new_records_per_increment

        self.i = 1
        self.actual_soa = self.soa_generator.next_add()
        self.all_records = []  # type: List[RRset]

    def __next__(self) -> ResponseManager:
        """Generate next zone changes."""
        if self.i > self.iterations:
            raise StopIteration()
        self.i += 1

        increment_content = []
        for _ in range(self.rand.randint(self.min_increments_per_step, self.max_increments_per_step)):
            # Put first SOA record to indicate the `remove` section of one incremental change
            increment_content.append(self.actual_soa)

            # Remove some records
            remove = self.rand.randint(self.min_remove_records_per_increment, self.max_remove_records_per_increment)
            for rr in (gen.next_remove() for gen in self.rand.choices(self.record_generators, k=remove) if gen.records):
                assert rr is not None  # Check to satisfy mypy. None values are already filtered in the generator above
                self.all_records.remove(rr)
                increment_content.append(rr)

            # Put second SOA record to indicate the `add` section of one incremental change
            self.actual_soa = self.soa_generator.next_add()
            increment_content.append(self.actual_soa)

            # Add new records
            count_of_records = self.rand.randint(self.min_new_records_per_increment, self.max_new_records_per_increment)
            for rr in (gen.next_add() for gen in self.rand.choices(self.record_generators, k=count_of_records)):
                self.all_records.append(rr)
                increment_content.append(rr)

        return DummyResponseManager(self.actual_soa, self.all_records, increment_content, self.rand)

    def get_init_data(self) -> ResponseManager:
        """Get data for initialization."""
        return AxfrResponseManager(self.actual_soa, [], self.rand)

    def get_zone(self) -> Zone:
        """Get actual zone."""
        return get_zone_from_records(self.domain, [self.actual_soa] + self.all_records)

    def require_zone(self) -> bool:
        """Require the zone check in the actual iteration."""
        return self.i % 2 == 1
