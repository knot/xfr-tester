# SPDX-License-Identifier: MPL-2.0
from random import Random
from typing import List

from dns.name import Name, from_text
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType
from dns.rdtypes.ANY.MX import MX
from dns.rdtypes.ANY.NS import NS
from dns.rdtypes.ANY.SOA import SOA
from dns.rdtypes.IN.A import A
from dns.rrset import RRset
from dns.zone import Zone

from xfr_tester.test_case.abstract import AbstractRecordGenerator, ResponseManager, TestCase
from xfr_tester.test_case.response_manager import (AxfrResponseManager, BasicResponseManager, DummyResponseManager,
                                                   OneByOneAxfrResponseManager)
from xfr_tester.utils import get_zone_from_records


class BasicARecordsTestCase(TestCase):
    """Test case for basic zone transfer using A records."""

    def __init__(self, domain: Name, iterations: int, rand: Random = Random(), ttl: int = 600, refresh: int = 600,
                 retry: int = 600, expire: int = 600, minimum: int = 600):
        """Init method."""
        self.domain = domain
        self.iterations = iterations
        self.rand = rand
        self.ttl = ttl
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum
        self.i = 1

    def get_init_data(self) -> ResponseManager:
        """Get initial data for testing in the form of a ``ResponseManager``."""
        return AxfrResponseManager(self._get_soa_rr(self.i), [self._get_rrset(self.i)], self.rand)

    def __next__(self) -> ResponseManager:
        """Generate next zone changes and therefore the new ``ResponseManager``."""
        if self.i > self.iterations:
            raise StopIteration()
        self.i += 1

        return BasicResponseManager(
            self._get_soa_rr(self.i - 1),
            self._get_soa_rr(self.i),
            [self._get_rrset(self.i)],
            [self._get_rrset(self.i - 1)],
            [self._get_rrset(self.i)],
            self.rand,
        )

    def _get_soa(self, serial: int) -> SOA:
        """Get actual SOA record."""
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text('admin', self.domain), serial, self.refresh,
                   self.retry, self.expire, self.minimum)

    def _get_soa_rr(self, serial: int) -> RRset:
        """Get ``RRset`` with the SOA record."""
        soa_rr = RRset(self.domain, RdataClass.IN, RdataType.SOA)
        soa_rr.add(self._get_soa(serial), self.ttl)
        return soa_rr

    def _get_rrset(self, group: int, num_of_records: int = 10) -> RRset:
        """Get ``RRset`` with records."""
        rr = RRset(self.domain, RdataClass.IN, RdataType.A)
        for i in range(num_of_records):
            rr.add(A(RdataClass.IN, RdataType.A, f'127.0.{group}.{i}'), self.ttl)

        return rr

    def get_zone(self) -> Zone:
        """Return actual zone."""
        zone = Zone(self.domain, relativize=False)
        soa_rr = zone.find_rdataset(self.domain, RdataType.SOA, create=True)
        soa_rr.add(self._get_soa(self.i), self.ttl)

        a_rr = zone.find_rdataset(self.domain, RdataType.A, create=True)
        for i in range(10):
            a_rr.add(A(RdataClass.IN, RdataType.A, f'127.0.{self.i}.{i}'), self.ttl)

        return zone

    def require_zone(self) -> bool:
        """Require the zone test just once in the middle."""
        return self.i == self.iterations // 2


class MultiIncrementalTestCase(TestCase):
    """Test case where each message is consisted of multiple zone increments."""

    def __init__(self, domain: Name, iterations: int, rand: Random = Random(), multiplication: int = 5, ttl: int = 600,
                 refresh: int = 600, retry: int = 600, expire: int = 600, minimum: int = 600,
                 mx_preference: int = 1000):
        """Init method."""
        self.domain = domain
        self.iterations = iterations
        self.rand = rand
        self.multiplication = multiplication
        self.ttl = ttl
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum
        self.mx_preference = mx_preference
        self.i = 1

    def _get_soa(self, serial: int) -> SOA:
        """Get actual SOA record."""
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text('admin', self.domain), serial, self.refresh,
                   self.retry, self.expire, self.minimum)

    def _get_soa_rr(self, serial: int) -> RRset:
        """Get ``RRset`` with the SOA record."""
        soa_rr = RRset(self.domain, RdataClass.IN, RdataType.SOA)
        soa_rr.add(self._get_soa(serial), self.ttl)
        return soa_rr

    def _get_rrsets(self, a: int, b: int) -> List[RRset]:
        """Get list of A and MX ``RRset``s."""
        a_record = A(RdataClass.IN, RdataType.A, f'127.0.{a}.{b}')
        rr_a = RRset(self.domain, RdataClass.IN, RdataType.A)
        rr_a.add(a_record, self.ttl)

        mx_record = MX(RdataClass.IN, RdataType.MX, self.mx_preference, from_text(f'mail.a{a}.b{b}', self.domain))
        rr_mx = RRset(self.domain, RdataClass.IN, RdataType.MX)
        rr_mx.add(mx_record, self.ttl)

        return [rr_a, rr_mx]

    def get_init_data(self) -> ResponseManager:
        """Get initial data for testing in the form of a ``ResponseManager``."""
        soa_rr = self._get_soa_rr((self.i + 1) * self.multiplication)
        rrs = self._get_rrsets(self.i + 1, 0)
        return AxfrResponseManager(soa_rr, rrs, self.rand)

    def __next__(self) -> ResponseManager:
        """Generate next zone changes and therefore the new ``ResponseManager``."""
        if self.i > self.iterations:
            raise StopIteration()
        self.i += 1

        # Remove records from the last iteration
        ixfr_content = [self._get_soa_rr(self.i * self.multiplication), *self._get_rrsets(self.i, 0)]

        # Create a mess in the middle
        for index in range(1, self.multiplication):
            ixfr_content.append(self._get_soa_rr(self.i * self.multiplication + index))
            ixfr_content.extend(self._get_rrsets(self.i, index))
            ixfr_content.append(self._get_soa_rr(self.i * self.multiplication + index))
            ixfr_content.extend(self._get_rrsets(self.i, index))

        # Add records for the new zone
        ixfr_content.append(self._get_soa_rr((self.i + 1) * self.multiplication))
        ixfr_content.extend(self._get_rrsets(self.i + 1, 0))

        soa_rr = self._get_soa_rr((self.i + 1) * self.multiplication)
        axfr_content = self._get_rrsets((self.i + 1), 0)
        return DummyResponseManager(soa_rr, axfr_content, ixfr_content, self.rand)

    def get_zone(self) -> Zone:
        """Return actual zone."""
        zone = Zone(self.domain, relativize=False)

        soa_rr = zone.get_rdataset(self.domain, RdataType.SOA, create=True)
        soa_rr.add(self._get_soa((self.i + 1) * self.multiplication), self.ttl)

        a_rr, mx_rr = self._get_rrsets(self.i + 1, 0)
        rd_a = zone.find_rdataset(self.domain, RdataType.A, create=True)
        rd_a.update(a_rr)
        rd_mx = zone.find_rdataset(self.domain, RdataType.MX, create=True)
        rd_mx.update(mx_rr)

        return zone


class SOAChangesOnlyTestCase(TestCase):
    """Changes made only from changes in SOA records."""

    def __init__(self, domain: Name, iterations: int, rand: Random = Random(), ttl: int = 600,
                 mx_preference: int = 1000):
        """Init method."""
        self.domain = domain
        self.iterations = iterations
        self.rand = rand
        self.ttl = ttl
        self.mx_preference = mx_preference
        self.i = 1

    def _get_soa(self, serial: int) -> SOA:
        """Get actual SOA record."""
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text(f'v{serial}', self.domain), serial,
                   600 + serial * 10, 600 + serial * 10, 600 + serial * 10, 600 + serial * 10)

    def _get_soa_rr(self, serial: int) -> RRset:
        """Get ``RRset`` with the SOA record."""
        soa_rr = RRset(self.domain, RdataClass.IN, RdataType.SOA)
        soa_rr.add(self._get_soa(serial), self.ttl)
        return soa_rr

    def _get_rrsets(self) -> List[RRset]:
        """Get ``RRset``s to be added to the zone forever."""
        rr_a = RRset(self.domain, RdataClass.IN, RdataType.A)
        rr_a.add(A(RdataClass.IN, RdataType.A, '127.0.0.1'), self.ttl)
        rr_a.add(A(RdataClass.IN, RdataType.A, '127.0.0.2'), self.ttl)
        rr_a.add(A(RdataClass.IN, RdataType.A, '127.0.0.3'), self.ttl)

        rr_mx = RRset(self.domain, RdataClass.IN, RdataType.MX)
        rr_mx.add(MX(RdataClass.IN, RdataType.MX, self.mx_preference * 1, from_text('mail', self.domain)), self.ttl)
        rr_mx.add(MX(RdataClass.IN, RdataType.MX, self.mx_preference * 2, from_text('mail', self.domain)), self.ttl)

        return [rr_a, rr_mx]

    def __next__(self) -> ResponseManager:
        """Generate next zone changes and therefore the new ``ResponseManager``."""
        if self.i > self.iterations:
            raise StopIteration()
        self.i += 1

        old_soa_rr = self._get_soa_rr(self.i - 1)
        new_soa_rr = self._get_soa_rr(self.i)
        rrs = self._get_rrsets()
        return BasicResponseManager(old_soa_rr, new_soa_rr, rrs, [], [], self.rand)

    def get_init_data(self) -> ResponseManager:
        """Get initial data for testing in the form of a ``ResponseManager``."""
        return AxfrResponseManager(self._get_soa_rr(self.i), self._get_rrsets(), self.rand)

    def get_zone(self) -> Zone:
        """Return actual zone."""
        zone = Zone(self.domain, relativize=False)

        soa_rr = zone.find_rdataset(self.domain, RdataType.SOA, create=True)
        soa_rr.add(self._get_soa(self.i), self.ttl)

        a_rr, mx_rr = self._get_rrsets()
        rd_a = zone.find_rdataset(self.domain, RdataType.A, create=True)
        rd_a.update(a_rr)
        rd_mx = zone.find_rdataset(self.domain, RdataType.MX, create=True)
        rd_mx.update(mx_rr)

        return zone


class NameOcclusionTestCase(TestCase):
    """Test case for name occlusion."""

    SUPPORTED_RD_TYPES = {
        RdataType.A: A,
        RdataType.NS: NS,
    }

    RD_TYPES_VALUES = {
        RdataType.A: lambda g, i: f'127.0.{g}.{i}',
        RdataType.NS: lambda g, i: Name(('ns', f'g{g}', f'i{i}', '')),
    }

    def __init__(self, domain: Name, iterations: int, rand: Random = Random(), ttl: int = 600, refresh: int = 600,
                 retry: int = 600, expire: int = 600, minimum: int = 600, num_of_static_records: int = 3,
                 num_of_dynamic_records: int = 5, num_of_ns_records: int = 2, static_label: str = 'static'):
        """Init method."""
        self.domain = domain
        self.iterations = iterations
        self.rand = rand
        self.ttl = ttl
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum
        self.num_of_static_records = num_of_static_records
        self.num_of_dynamic_records = num_of_dynamic_records
        self.num_of_ns_records = num_of_ns_records

        self.i = 1
        self.static_domain = Name((static_label,)).concatenate(self.domain)

    def __next__(self) -> ResponseManager:
        """Generate next zone changes and therefore the new ``ResponseManager``."""
        if self.i > self.iterations:
            raise StopIteration()
        self.i += 1

        old_soa_rr = self._get_soa_rr(self.i - 1)
        new_soa_rr = self._get_soa_rr(self.i)
        rrs = self._get_actual_rrs()
        remove = [
            self._get_rrset(self.domain, self.i - 1, RdataType.NS, self.num_of_ns_records),
            self._get_rrset(self._get_dynamic_domain(self.i - 1), self.i - 1, RdataType.A, self.num_of_dynamic_records),
        ]
        add = [
            self._get_rrset(self.domain, self.i, RdataType.NS, self.num_of_ns_records),
            self._get_rrset(self._get_dynamic_domain(self.i), self.i, RdataType.A, self.num_of_dynamic_records),
        ]

        return BasicResponseManager(old_soa_rr, new_soa_rr, rrs, remove, add, self.rand)

    def _get_dynamic_domain(self, index: int):
        """Get domain name with the dynamic subdomain."""
        return Name(('dyn', f'i{index}')).concatenate(self.domain)

    def get_init_data(self) -> ResponseManager:
        """Get initial data for testing in the form of a ``ResponseManager``."""
        return AxfrResponseManager(self._get_soa_rr(self.i), self._get_actual_rrs(), self.rand)

    def get_zone(self) -> Zone:
        """Return actual zone."""
        zone = Zone(self.domain, relativize=False)

        soa_rr = zone.find_rdataset(self.domain, RdataType.SOA, create=True)
        soa_rr.add(self._get_soa(self.i), self.ttl)
        ns_rr = zone.find_rdataset(self.domain, RdataType.NS, create=True)
        ns_rr.update(self._get_rrset(self.domain, self.i, RdataType.NS, self.num_of_ns_records))

        a_rr = zone.find_rdataset(self.static_domain, RdataType.A, create=True)
        a_rr.update(self._get_rrset(self.static_domain, 0, RdataType.A, self.num_of_static_records))

        dynamic_domain = self._get_dynamic_domain(self.i)
        a_rr = zone.find_rdataset(dynamic_domain, RdataType.A, create=True)
        a_rr.update(self._get_rrset(dynamic_domain, self.i, RdataType.A, self.num_of_dynamic_records))

        return zone

    def _get_actual_rrs(self) -> List[RRset]:
        """Get actual ``RRset``s."""
        rrs = [
            self._get_rrset(self.domain, self.i, RdataType.NS, self.num_of_ns_records),
            self._get_rrset(self.static_domain, 0, RdataType.A, self.num_of_static_records),
            self._get_rrset(self._get_dynamic_domain(self.i), self.i, RdataType.A, self.num_of_dynamic_records),
        ]
        return rrs

    def _get_soa(self, version: int) -> SOA:
        """Get next SOA record."""
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text('admin', self.domain), version, self.refresh,
                   self.retry, self.expire, self.minimum)

    def _get_soa_rr(self, serial: int) -> RRset:
        """Get ``RRset`` with the SOA record."""
        soa_rr = RRset(self.domain, RdataClass.IN, RdataType.SOA)
        soa_rr.add(self._get_soa(serial), self.ttl)
        return soa_rr

    def _get_rrset(self, domain: Name, group: int, rdtype: RdataType, num_of_records: int = 10) -> RRset:
        """Get ``RRset`` with records."""
        rr = RRset(domain, RdataClass.IN, rdtype)
        for i in range(1, num_of_records + 1):
            rr.add(self.SUPPORTED_RD_TYPES[rdtype](RdataClass.IN, rdtype, self.RD_TYPES_VALUES[rdtype](group, i)),
                   self.ttl)

        return rr


class SerialNumberArithmeticTestCase(TestCase):
    """Test case for serial number arithmetics."""

    SERIAL_BITS = 32

    def __init__(self, domain: Name, start: int, step: int, iterations: int, rand: Random = Random(), ttl: int = 600,
                 refresh: int = 600, retry: int = 600, expire: int = 600, minimum: int = 600):
        """Init method."""
        assert 0 < step <= 2 ** (self.SERIAL_BITS - 1) - 1
        self.domain = domain
        self.start = start
        self.step = step
        self.iterations = iterations
        self.rand = rand
        self.ttl = ttl
        self.refresh = refresh
        self.retry = retry
        self.expire = expire
        self.minimum = minimum

        self.i = 0
        self.send_fake = False

    def _get_soa(self, serial: int) -> SOA:
        """Get next SOA record."""
        return SOA(RdataClass.IN, RdataType.SOA, self.domain, from_text('admin', self.domain), serial, self.refresh,
                   self.retry, self.expire, self.minimum)

    def _get_soa_rr(self, serial: int) -> RRset:
        """Get ``RRset`` with the SOA record."""
        soa_rr = RRset(self.domain, RdataClass.IN, RdataType.SOA)
        soa_rr.add(self._get_soa(serial), self.ttl)
        return soa_rr

    def _compute_serial(self, version: int) -> int:
        """Compute new serial number."""
        return (self.start + version * self.step) % (2 ** self.SERIAL_BITS)

    def get_init_data(self) -> ResponseManager:
        """Get initial data for testing in the form of a ``ResponseManager``."""
        return AxfrResponseManager(self._get_soa_rr(self.start), [], self.rand)

    def __next__(self) -> ResponseManager:
        """Generate next zone changes and therefore the new ``ResponseManager``."""
        if self.i >= self.iterations:
            raise StopIteration()

        if self.send_fake:
            old_serial = self._compute_serial(self.i - 1)
            new_serial = self._compute_serial(self.i) - 1
        else:
            old_serial = self._compute_serial(self.i)
            new_serial = self._compute_serial(self.i + 1)
            self.i += 1

        self.send_fake = not self.send_fake
        old_serial_rr = self._get_soa_rr(old_serial)
        new_serial_rr = self._get_soa_rr(new_serial)
        return BasicResponseManager(old_serial_rr, new_serial_rr, [], [], [], self.rand)

    def get_zone(self) -> Zone:
        """Return actual zone."""
        zone = Zone(self.domain, relativize=False)
        soa_rr = zone.find_rdataset(self.domain, RdataType.SOA, create=True)
        soa_rr.add(self._get_soa(self._compute_serial(self.i)), self.ttl)
        return zone

    def require_zone(self) -> bool:
        """Always require the zone check."""
        return True


class SingleRecordTestCase(TestCase):
    """Test which just removes and adds the same records in each iteration."""

    def __init__(
            self,
            domain: Name,
            soa_generator: AbstractRecordGenerator,
            records: List[RRset],
            other_records: List[RRset] = None,
            iterations: int = 5,
            rand: Random = Random(),
    ):
        """Init method."""
        self.domain = domain
        self.soa_generator = soa_generator
        self.records = records
        self.other_records = other_records or []
        self.iterations = iterations
        self.rand = rand

        self.i = 1
        self.actual_soa = self.soa_generator.next_add()

    def __next__(self) -> ResponseManager:
        """Remove ``records`` and add them back right away."""
        if self.i >= self.iterations:
            raise StopIteration()
        self.i += 1

        old_soa = self.actual_soa
        self.actual_soa = self.soa_generator.next_add()

        return BasicResponseManager(old_soa, self.actual_soa, self.other_records + self.records, self.records,
                                    self.records, self.rand)

    def get_init_data(self) -> ResponseManager:
        """Get initial data, which are the ``other_records`` together with ``records`` used for testing."""
        return AxfrResponseManager(self.actual_soa, self.other_records + self.records, self.rand)

    def get_zone(self) -> Zone:
        """Get zone, it means the ``records`` with ``other_records`` and the SOA record."""
        return get_zone_from_records(self.domain, [self.actual_soa] + self.records + self.other_records)


class SingleRecordAxfrTestCase(TestCase):
    """Send AXFR results with a zone, which has just one record."""

    def __init__(
            self,
            domain: Name,
            soa_records_generator: AbstractRecordGenerator,
            iterations: int = 10,
            rand: Random = Random(),
    ):
        """Init method."""
        self.domain = domain
        self.soa_records_generator = soa_records_generator
        self.iterations = iterations
        self.rand = rand

        self.actual_soa = self.soa_records_generator.next_add()
        self.i = 1

    def get_init_data(self) -> ResponseManager:
        """Init the zone with 10 records, which will be immediately removed."""
        rrset = RRset(self.domain, RdataClass.IN, RdataType.A)
        for i in range(10):
            rrset.add(A(RdataClass.IN, RdataType.A, f'127.0.0.{i}'), 600)
        return AxfrResponseManager(self.actual_soa, [rrset], self.rand)

    def __next__(self) -> ResponseManager:
        """Generate another change."""
        if self.i >= self.iterations:
            raise StopIteration()
        self.i += 1

        self.actual_soa = self.soa_records_generator.next_add()
        rrset = RRset(self.domain, RdataClass.IN, RdataType.A)
        rrset.add(A(RdataClass.IN, RdataType.A, f'127.0.{self.i}.1'), 700)

        return OneByOneAxfrResponseManager(self.actual_soa, [rrset], self.rand)

    def get_zone(self) -> Zone:
        """Return the zone during increments, does not work for the initial."""
        rrset = RRset(self.domain, RdataClass.IN, RdataType.A)
        rrset.add(A(RdataClass.IN, RdataType.A, f'127.0.{self.i}.1'), 700)
        return get_zone_from_records(self.domain, [self.actual_soa, rrset])

    def require_zone(self) -> bool:
        """Always require the zone."""
        return True
