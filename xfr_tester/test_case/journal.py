# SPDX-License-Identifier: MPL-2.0
from pathlib import Path
from random import Random
from typing import Generator, List, Tuple

from dns.name import Name, from_text as name_from_text
from dns.rdata import get_rdata_class
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType, from_text as rdtype_from_text
from dns.rrset import RRset
from dns.tokenizer import Tokenizer
from dns.ttl import from_text as ttl_from_text
from dns.zone import Zone

from xfr_tester.test_case.abstract import ResponseManager, TestCase
from xfr_tester.test_case.response_manager import AxfrResponseManager, BasicResponseManager
from xfr_tester.utils import get_zone_from_records


class JournalException(Exception):
    """An exception while reading the journal file."""


def kjournalprint_reader(file: Path, rdclass: RdataClass = RdataClass.IN) -> Generator[RRset, None, None]:
    """Reader of ``kjournalprint`` logs."""
    with file.open() as f:
        for line in f:
            if line[:2] == ';;':  # A comment
                continue

            _domain, _ttl, _rdtype, value = line[:-1].split(maxsplit=3)

            domain = name_from_text(_domain)
            ttl = ttl_from_text(_ttl)
            rdtype = rdtype_from_text(_rdtype)

            rdata_cls = get_rdata_class(rdclass, rdtype)
            record = rdata_cls.from_text(rdclass, rdtype, Tokenizer(value))

            rrset = RRset(domain, rdclass, rdtype)
            rrset.add(record, ttl)
            yield rrset


class JournalTestCase(TestCase):
    """Read increments from a journal file."""

    def __init__(self, domain: Name, reader: Generator[RRset, None, None], rand: Random = Random()):
        """
        Init method.

        Args:
            domain (Name): The domain name.
            reader (Generator[RRset, None, None]): Generator if records.
            rand (Random): Random instance.
        """
        self.domain = domain
        self.reader = reader
        self.rand = rand
        self.all_records = []  # type: List[RRset]

        self._finished = False
        self._actual_soa = next(self.reader)
        if self._actual_soa.rdtype != RdataType.SOA:
            raise JournalException('The first record is supposed to be the SOA record.')

    def _get_section(self) -> Tuple[RRset, List[RRset]]:
        """Get next section in the form of SOA RRset and list of RRsets in the section, or raise StopIteration."""
        if self._finished is True:
            raise StopIteration()

        soa_rrset = self._actual_soa
        rrsets = []
        for rrset in self.reader:
            if rrset.rdtype == RdataType.SOA:
                self._actual_soa = rrset
                break
            rrsets.append(rrset)
        else:
            self._finished = True

        return soa_rrset, rrsets

    def get_init_data(self) -> ResponseManager:
        """Get init data, first section from the journal."""
        soa_rrset, rrsets = self._get_section()
        self.all_records.extend(rrsets)
        return AxfrResponseManager(soa_rrset, rrsets, self.rand)

    def __next__(self) -> ResponseManager:
        """Increment, other two sections from the journal."""
        rem_soa_rrset, rem_rrsets = self._get_section()
        add_soa_rrset, add_rrsets = self._get_section()

        for rrset in rem_rrsets:
            self.all_records.remove(rrset)
        self.all_records.extend(add_rrsets)

        return BasicResponseManager(rem_soa_rrset, add_soa_rrset, self.all_records, rem_rrsets, add_rrsets, self.rand)

    def get_zone(self) -> Zone:
        """Get zone version."""
        return get_zone_from_records(self.domain, [self._actual_soa] + self.all_records)

    def require_zone(self) -> bool:
        """Require zone check."""
        return True
