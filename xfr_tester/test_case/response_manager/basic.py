# SPDX-License-Identifier: MPL-2.0
from random import Random
from typing import Callable, Dict, List

from dns.exception import TooBig
from dns.message import Message, make_response
from dns.name import root
from dns.rcode import Rcode
from dns.rdataclass import RdataClass
from dns.rdatatype import RdataType
from dns.rrset import RRset

from xfr_tester.datatypes import BytesMessage, ResponseManagerResult
from xfr_tester.test_case.abstract import ResponseManager


class DummyResponseManager(ResponseManager):
    """Dummy response manager for returning lists of content."""

    def __init__(self, soa_rr: RRset, axfr_content: List[RRset], ixfr_content: List[RRset], rand: Random = Random()):
        """
        Init method.

        Args:
            soa_rr (RRset): RRset with the actual SOA record.
            axfr_content (List[RRset]): List of record to be used as AXFR response.
            ixfr_content (List[RRset]): List of record to be used as IXFR response.
            rand (Random): Random instance for test replication.
        """
        assert soa_rr.rdtype == RdataType.SOA, 'Expected SOA record type.'
        assert soa_rr.rdclass == RdataClass.IN, 'Expected SOA record class.'
        assert len(soa_rr) == 1, 'SOA RRset must contain exactly one record.'
        self.soa_rr = soa_rr
        self.axfr_content = axfr_content
        self.ixfr_content = ixfr_content
        self.rand = rand

    def get_soa_rr(self) -> RRset:
        """Get actual SOA ``RRset``."""
        return self.soa_rr

    def get_axfr_content(self):
        """Get AXFR content for the response."""
        return self.axfr_content

    def get_ixfr_content(self):
        """Get IXFR content for the response."""
        return self.ixfr_content

    def get_soa_response(self, request: Message) -> ResponseManagerResult:
        """Get response for the SOA query."""
        response = make_response(request)
        response.answer.append(self.soa_rr)
        return ResponseManagerResult([response], False)

    def get_axfr_response(self, request: Message) -> ResponseManagerResult:
        """Get response for the AXFR query."""
        all_records = [self.soa_rr, *self.get_axfr_content(), self.soa_rr]
        return ResponseManagerResult(self._get_messages(request, all_records), False)

    def get_ixfr_response(self, request: Message) -> ResponseManagerResult:
        """Get response for the IXFR query."""
        all_records = [self.soa_rr, *self.get_ixfr_content(), self.soa_rr]
        return ResponseManagerResult(self._get_messages(request, all_records), False)

    def _get_messages(self, request: Message, answer: List[RRset]) -> List[BytesMessage]:
        """Split one big answer in form of ``Message`` into small-enough messages in form of ``bytes``."""
        response = make_response(request)
        response.answer.extend(answer)
        try:
            return [response.to_wire(root)]
        except TooBig:
            records = len(response.answer)
            assert records >= 2, f'The response has only {records}, but is too long.'
            index = self.rand.randint(max([records // 2, 1]), records - 1)
            st, nd = response.answer[:index], response.answer[index:]
            return [*self._get_messages(request, st), *self._get_messages(request, nd)]

    @staticmethod
    def get_default_response(request: Message) -> ResponseManagerResult:
        """Prepare response message from the request message."""
        response = make_response(request)
        response.rcode(Rcode.NOTIMP)
        return ResponseManagerResult([response], False)

    def get_responses(self, request: Message) -> ResponseManagerResult:
        """Get response manager for the request."""
        responses_rdtype_map = {
            RdataType.SOA: self.get_soa_response,
            RdataType.AXFR: self.get_axfr_response,
            RdataType.IXFR: self.get_ixfr_response,
        }  # type: Dict[RdataType, Callable[[Message], ResponseManagerResult]]
        return responses_rdtype_map.get(request.question[0].rdtype, self.get_default_response)(request)


class AxfrResponseManager(DummyResponseManager):
    """Response manager returning AXFR content on IXFR queries."""

    def __init__(self, soa_rr: RRset, rrs: List[RRset], rand: Random = Random()):
        """Init method."""
        super().__init__(soa_rr, rrs, [], rand)

    def get_ixfr_content(self):
        """Get response for the IXFR query in form of AXFR response."""
        return self.get_axfr_content()


class OneByOneAxfrResponseManager(AxfrResponseManager):
    """Response, where N RRsets in the Answer section are split into N messages."""

    def _get_messages(self, request: Message, answer: List[RRset]) -> List[BytesMessage]:
        """Get separated message for every RRset in the Answer section."""
        ret = []
        for rrset in answer:
            response = make_response(request)
            response.answer.append(rrset)
            ret.append(response)
        return ret


class BasicResponseManager(DummyResponseManager):
    """
    Basic response manager.

    This response manager generates responses only from old and new SOA records, list of all records and lists of
    records to be added and removed.
    """

    def __init__(
            self,
            old_soa_rr: RRset,
            new_soa_rr: RRset,
            rrs: List[RRset],
            remove: List[RRset],
            add: List[RRset],
            rand: Random = Random()
    ):
        """
        Init method.

        Args:
            old_soa_rr (RRset): RRset with the old SOA record.
            new_soa_rr (RRset): RRset with the new SOA record.
            rrs (List[RRset]): List of all records to be returned as AXFR response.
            remove (List[RRset]): List of records to be added into the remove section of IXFR response.
            add (List[RRset]): List of records to be added into the add section of IXFR response.
            rand (Random): Random instance for test replication.
        """
        assert old_soa_rr.rdtype == RdataType.SOA, 'Expected SOA record type.'
        assert old_soa_rr.rdclass == RdataClass.IN, 'Expected SOA record class.'
        assert len(old_soa_rr) == 1, 'SOA RRset must contain exactly one record.'
        ixfr_response = [old_soa_rr, *remove, new_soa_rr, *add]
        super().__init__(new_soa_rr, rrs, ixfr_response, rand)
        self.old_soa_rr = old_soa_rr

    def _check_ixfr_request_serial(self, request: Message) -> bool:
        """Return whether or not is SOA record correct (same as expected) in the request."""
        request_soa_serial = request.authority[0][0].serial  # first index for RRset, second index for record
        old_soa_serial = self.old_soa_rr[0].serial  # index for record in the RRset
        return request_soa_serial == old_soa_serial

    def get_ixfr_response(self, request: Message) -> ResponseManagerResult:
        """Return IXFR response if and only if the requested SOA serial number is correct."""
        if not self._check_ixfr_request_serial(request):
            return self.get_axfr_response(request)
        return super().get_ixfr_response(request)
