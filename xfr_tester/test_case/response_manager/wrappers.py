# SPDX-License-Identifier: MPL-2.0
from dns.message import Message
from dns.rdatatype import RdataType
from dns.rrset import RRset

from xfr_tester.datatypes import ResponseManagerResult
from xfr_tester.test_case.abstract import ResponseManager


class BlackoutResponseManager(ResponseManager):
    """Wrapper for imitating blackout during the zone transfer."""

    def __init__(self, sub_rm: ResponseManager):
        """
        Init method.

        Args:
            sub_rm (ResponseManager): Response manager to be transfered during the blackout.
        """
        self.sub_rm = sub_rm

    def get_responses(self, request: Message) -> ResponseManagerResult:
        """Break the responses and simulate a blackout."""
        if request.question[0].rdtype not in (RdataType.AXFR, RdataType.IXFR):
            return self.sub_rm.get_responses(request)

        responses, _ = self.sub_rm.get_responses(request)
        middle_index = len(responses) // 2

        blackout_message = responses[middle_index]
        if isinstance(blackout_message, Message):
            blackout_message = blackout_message.to_wire()
        assert isinstance(blackout_message, bytes)
        blackout_message = blackout_message[:len(blackout_message) // 2]

        return ResponseManagerResult(responses[:middle_index] + [blackout_message], True)

    def get_soa_rr(self) -> RRset:
        """Get ``RRset`` with the SOA record."""
        return self.sub_rm.get_soa_rr()
