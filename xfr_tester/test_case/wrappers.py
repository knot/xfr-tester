# SPDX-License-Identifier: MPL-2.0
from dns.name import root
from dns.zone import Zone

from xfr_tester.test_case.abstract import ResponseManager, TestCase
from xfr_tester.test_case.response_manager import BlackoutResponseManager


class XfrBlackoutTestCase(TestCase):
    """Test case simulating a blackout."""

    def __init__(self, subtest: TestCase):
        """
        Init method.

        Args:
            subtest (TestCase): Test case to be using for the blackout simulation.
        """
        self.subtest = subtest
        self.actual_zone = Zone(root)

    def __iter__(self):
        """Get the generator."""
        self.actual_zone = self.subtest.get_zone()
        for sub_result in self.subtest:
            yield BlackoutResponseManager(sub_result)
            self.actual_zone = self.subtest.get_zone()
            yield sub_result

    def __next__(self) -> ResponseManager:
        """We use ``__iter__`` method for these purposes."""
        raise NotImplementedError(
            'This TestCase uses another TestCase as the generator. Do not access next() directly.')

    def require_zone(self) -> bool:
        """Require zone check if the subtest requires the zone check."""
        return self.subtest.require_zone()

    def get_init_data(self) -> ResponseManager:
        """Return init data of the subtest."""
        return self.subtest.get_init_data()

    def get_zone(self) -> Zone:
        """Return the subtest zone."""
        return self.actual_zone
