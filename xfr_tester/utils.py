# SPDX-License-Identifier: MPL-2.0
import string
from random import Random
from typing import List

from dns.name import Name, root
from dns.rrset import RRset
from dns.zone import Zone


def rand_ttl(rand: Random = None, min_value: int = 0, max_value: int = 2 ** 31 - 1) -> int:
    """
    Get random TTL value.

    Args:
        rand (Random): Random instance for test replication.
        min_value (int): Minimal value.
        max_value (int): Maximal value.

    Returns:
        int: Random TTL.
    """
    rand = rand or Random()
    return rand.randint(min_value, max_value)


def rand_label(length: int, rand: Random = Random()) -> str:
    """
    Get random domain label.

    Args:
        length (int): Length of the label.
        rand (Random): Random instance for test replication.

    Returns:
        str: Random domain label.
    """
    first = rand.choice(string.ascii_lowercase)
    middle = ''.join(rand.choices(string.ascii_lowercase + string.digits + '-', k=length - 2))
    last = rand.choice(string.ascii_lowercase + string.digits)
    return (first + middle + last)[:length]


def rand_name(origin: Name = root, max_length: int = None, rand: Random = Random()) -> Name:
    """
    Get random domain name.

    The length of the name is 'text length' plus 1, because of the last root byte!

    Args:
        origin (Name): The parent domain, where the subdomains will be prepended.
        max_length (int): Max length of the whole domain.
        rand (Random): Random instance for test replication.

    Returns:
        Name: Randomized domain name.
    """
    curr_len = len(origin.to_text()) + 1 if origin != root else 1
    max_length = max_length or rand.randint(curr_len + 2, 254)

    labels = []
    while curr_len < max_length:
        label_len = min([max_length - curr_len - 1, rand.randint(1, 63)])
        if max_length - curr_len - label_len - 1 == 1:  # It's a trap, not enough space for the last label!
            if label_len == 63:  # We cannot enlarge the label, just generate another label length
                continue
            else:  # Or we can enlarge the label length by the missing one byte
                label_len += 1
        label = rand_label(label_len, rand)
        labels.append(label)
        curr_len += label_len + 1

    return Name(labels).concatenate(origin)


def get_zone_from_records(domain: Name, records: List[RRset]):
    """Build a ``Zone`` from records."""
    zone = Zone(domain, relativize=False)

    for rrset in records:
        node = zone.find_node(rrset.name, create=True)
        rdataset = node.find_rdataset(rrset.rdclass, rrset.rdtype, create=True)
        rdataset.update(rrset)

    return zone
